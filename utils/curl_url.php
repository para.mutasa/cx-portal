<?php
$base_url = "http://192.168.84.146:31659/";

//Configs API
$create_config = 'configs/create';
$update_config = 'configs/update';
$all_configs = 'configs/find-all-configs';

//User  API
$create_user = 'user/create';
$update_user = 'user/update';
$admin_login = 'user/login';
$all_users = 'user/find-all-users';
$forgot_password ='user/forgot-password';
$reset_password ='user/reset-password';
$change_password = 'user/change-password-on-first-login';

//Survey  API
$create_survey = 'survey/create';
$update_survey = 'survey/update';
$all_surveys = 'survey/find-all';

//Services API
$create_service = 'service/create';
$update_service = 'service/update';
$all_services= 'service/find-all-services';
$cred_service = 'account/find-account-by-service-name/';
$send_cred = 'account/send-credentials/';

//Questions  API
$create_question = 'questions/create';
$update_question = 'questions/update';
$get_survey_questions = 'questions/find-survey-questions';
$get_all_questions = 'questions/find-all';
$get_all_questions_by_id = 'questions/find-all-by-survey-id/';

//Responses  API
$save_responses = 'responses/save-responses';
$user_responses = 'reports/find-all-by-startDate-and-endDate/';

//rate limiting Api

$all_limits = 'rate-limit-configs/find-all-configs';
$create_limit = 'rate-limit-configs/create';
$delete_limit = 'rate-limit-configs/remove/';

?>
