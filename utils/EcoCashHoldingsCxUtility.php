<?php
    error_reporting(E_ALL);
    include_once('curl_url.php');
    $GOOGLE_API_KEY  = "AIzaSyDQTpHIDA-oUPXuJhXW7PHxRo_9OWUnoMs";
    $secret = '6LeUkr8ZAAAAAPLjUQfjU8takt8g7rxonze_GVIO';
    $key = "tHeApAcHe6410111";
    
    function getAuthToken(){
        global $base_url;
        $curl = curl_init();
        curl_setopt_array($curl, [
          CURLOPT_URL => $base_url.'v1/token?scope=read&username=user_test&password=p_%4055_304_yt&client_id=edx2c30lorg&client_secret=1op9tfxy7&grant_type=password',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "",
        ]);
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        
        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
            $res_arr = json_decode($response,false);
            return $res_arr->access_token;
        }
        }


    function callWebApiPost($jsonData, $curl_url){
        $ch = curl_init($curl_url);
        $jsonDataEncoded = json_encode($jsonData);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        $ACC_TOKEN = getAuthToken();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'authorization: Bearer '.$ACC_TOKEN));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);

        return $result;
    }

    function callWebApiDelete($curl_url){
        $ch = curl_init($curl_url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        $ACC_TOKEN = getAuthToken();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'authorization: Bearer '.$ACC_TOKEN));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);

        return $result;
    }

    function callWebApiGet($curl_url){
        $ch = curl_init($curl_url);
        $ACC_TOKEN = getAuthToken();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'authorization: Bearer '.$ACC_TOKEN));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    function callWebApiPOSTEmail($curl_url){
        $ch = curl_init($curl_url);
        curl_setopt($ch, CURLOPT_POST, 1);
        $ACC_TOKEN = getAuthToken();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'authorization: Bearer '.$ACC_TOKEN));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    function encrypt($data, $key) {
        return base64_encode(openssl_encrypt($data, "aes-128-ecb", $key, OPENSSL_RAW_DATA));
    }
    
    function decrypt($data, $key) {
        return openssl_decrypt(base64_decode($data), "aes-128-ecb", $key, OPENSSL_RAW_DATA);
    }
        
 /* Start Questions */

    //get all survey questions
     function getAllSurveyQuestions(){
        global $base_url;
        global $get_survey_questions;

        return callWebApiGet($base_url.$get_survey_questions);
     }


function getAllSurveyQuestionsById($surveyId){
    global $base_url;
    global $get_all_questions_by_id;
    return callWebApiGet($base_url.$get_all_questions_by_id.$surveyId);

}

 /* End Questions */

  /* Start Responses */
        //save  responses

        function saveAllResponses($uuid,$serviceName,$questionIdOne,$responseOne,$additionalTextOne,$questionIdTwo,$responseTwo,$additionalTextTwo,$questionIdThree,$responseThree,$additionalTextThree,$questionIdFour,$responseFour,$additionalTextFour){
           
            $jsonData = array(
                'uuid' => $uuid,
                'serviceName' => $serviceName,
               'questionIdOne' => $questionIdOne,
                'responseOne' => $responseOne,
                'additionalTextOne' => $additionalTextOne,
                'questionIdTwo' => $questionIdTwo,
                'responseTwo' => $responseTwo,
                'additionalTextTwo' => $additionalTextTwo,
                'questionIdThree' => $questionIdThree,
                'responseThree' => $responseThree,
                'additionalTextThree' => $additionalTextThree,
                'questionIdFour' => $questionIdFour,
                'responseFour' => $responseFour,
                'additionalTextFour' => $additionalTextFour
           
            );

            global $base_url;
            global $save_responses;
            return callWebApiPost($jsonData,$base_url.$save_responses);

    }

/* End Responses  */

function adminLogin($email,$password)
{
  $jsonData = array(
    'email' => $email,
    'password' => $password
  );
  global $base_url;
  global $admin_login;
  return callWebApiPost($jsonData,$base_url.$admin_login);
}


function getAllQuestions(){
    global $base_url;
    global $get_all_questions;

    return callWebApiGet($base_url.$get_all_questions);
 }

 function getAllSurvey(){
    global $base_url;
    global $all_surveys;

    return callWebApiGet($base_url.$all_surveys);
 }
 function getAllServices(){
    global $base_url;
    global $all_services;

    return callWebApiGet($base_url.$all_services);
 }

     function addSurvey($surveyName,$status){

        $jsonData = array(
            'surveyName' => $surveyName,
            'status' => $status
        );

        global $base_url;
        global $create_survey;
        return callWebApiPost($jsonData,$base_url.$create_survey);

    }


    function addService($serviceName,$status,$createdBy){

        $jsonData = array(
            'serviceName' => $serviceName,
            'status' => $status,
            'createdBy' => $createdBy
        );

        global $base_url;
        global $create_service;
        return callWebApiPost($jsonData,$base_url.$create_service);

    }

    function getAllConfigs(){
        global $base_url;
        global $all_configs;
    
        return callWebApiGet($base_url.$all_configs);
     }

     function getAllUsers(){
        global $base_url;
        global $all_users;
    
        return callWebApiGet($base_url.$all_users);
     }

     function getAllLimits(){
        global $base_url;
        global $all_limits;
    
        return callWebApiGet($base_url.$all_limits);
     }
     function addLimit($configId,$rate,$perNumberOfDays,$lastModifiedBy,$createdBy){
    
        $jsonData = array(
            'configId' => $configId,
            'rate' => $rate,
            'perNumberOfDays' => $perNumberOfDays,
            'lastModifiedBy' => $lastModifiedBy,
            'createdBy' => $createdBy,
            
            
        );

        global $base_url;
        global $create_limit;
        return callWebApiPost($jsonData,$base_url.$create_limit);

    }

//delete limit       
function deleteLimit($configId){          
    global $base_url;           
    global $delete_limit;            
    return callWebApiDelete($base_url.$delete_limit.$configId);       
    }


     function addConfig($name,$value){

        $jsonData = array(
            'name' => $name,
            'value' => $value
        );

        global $base_url;
        global $create_config;
        return callWebApiPost($jsonData,$base_url.$create_config);

    }

    function addUser($userName,$firstName,$lastName,$email,$mobileNumber,$accessType,$createdBy){

        $jsonData = array(
            'userName' => $userName,
            'firstName' => $firstName,
            'lastName' => $lastName,
            'email' => $email,
            'mobileNumber' => $mobileNumber,
            'accessType' => $accessType,
            'createdBy'=> $createdBy
        );

        global $base_url;
        global $create_user;
        return callWebApiPost($jsonData,$base_url.$create_user);

    }
    function forgotPassword($email){

        $jsonData = array(
            
            'email' => $email
        
        );

        global $base_url;
        global $forgot_password;
        return callWebApiPost($jsonData,$base_url.$forgot_password);

    }
    function resetPassword($email,$resetToken,$newpassword){

        $jsonData = array(
            
            'email' => $email,
            'resetToken' => $resetToken,
            'newpassword' => $newpassword
        
        );

        global $base_url;
        global $reset_password;
        return callWebApiPost($jsonData,$base_url.$reset_password);

    }


    function createQuestion($surveyId,$questionNumber,$question,$createdBy){

        $jsonData = array(
            'surveyId' => $surveyId,
            'questionNumber' => $questionNumber,
            'question' => $question,
            'createdBy' => $createdBy
        );
        global $base_url;
        global $create_question;
        return callWebApiPost($jsonData,$base_url.$create_question);

    }

    function editSurvey($surveyId,$surveyName,$status,$lastModifiedBy){
        
        $jsonData = array(
            'surveyId' => $surveyId,
            'surveyName' => $surveyName,
            'status' => $status,
            'lastModifiedBy' => $lastModifiedBy
        );
        global $base_url;
        global $update_survey;
        return callWebApiPost($jsonData,$base_url.$update_survey);

    }
    
    
function editQuestion($questionId,$surveyId,$questionNumber,$question,$lastModifiedBy){
        
    $jsonData = array(
        'questionId'=>$questionId,
        'surveyId' => $surveyId,
        'questionNumber' => $questionNumber,
        'question' => $question,
        'lastModifiedBy' => $lastModifiedBy
    );
    global $base_url;
    global $update_question;
    return callWebApiPost($jsonData,$base_url.$update_question);

}
    function editUser($status,$userId,$userName,$firstName,$lastName,$accessType,$email,$mobileNumber,$lastModifiedBy){
        
        $jsonData = array(
            'status' => $status,
            'userId' => $userId,
            'userName' => $userName,
            'firstName' => $firstName,
            'lastName' => $lastName,
            'accessType' => $accessType,       
            'email' => $email,
            'mobileNumber' => $mobileNumber,
            'lastModifiedBy' => $lastModifiedBy
           
        );
        global $base_url;
        global $update_user;
        return callWebApiPost($jsonData,$base_url.$update_user);

    }

    function changePassword($email,$oldPassword,$password){

        $jsonData = array(
            
            'email' => $email,
            'oldPassword' => $oldPassword,
            'newPassword' => $password
        
        );

        global $base_url;
        global $change_password;
        return callWebApiPost($jsonData,$base_url.$change_password);

    }
    function editService($serviceId,$serviceName,$status,$ratingLimit,$lastModifiedBy){

        $jsonData = array(
            'serviceId' => $serviceId,
            'serviceName' => $serviceName,
            'status' => $status,
            'ratingLimit' => $ratingLimit,
            'lastModifiedBy' => $lastModifiedBy
        );
        global $base_url;
        global $update_service;
        return callWebApiPost($jsonData,$base_url.$update_service);

    }

    function credService($serviceName){

        global $base_url;
        global $cred_service;
        return callWebApiGet($base_url.$cred_service.'/'.$serviceName);
    
    }

    function  editConfig($configId,$name,$value,$lastModifiedBy){

        $jsonData = array(
            'configId' => $configId,
            'name' => $name,
            'value' => $value,
            'lastModifiedBy' => $lastModifiedBy
        );
        global $base_url;
        global $update_config;
        return callWebApiPost($jsonData,$base_url.$update_config);

    }

    function getAllResponses($startDate,$endDate){

        global $base_url;
        global $user_responses;
        return callWebApiGet($base_url.$user_responses.'/'.$startDate.'/'.$endDate);
    
    }

    function sendCred($serviceName,$email){

        global $base_url;
        global $send_cred;
        return callWebApiPOSTEmail($base_url.$send_cred.'/'.$serviceName.'/'.$email);
    
    }

   // use this to search by startDate,endDate,msisdn,serviceName
    // function getAllResponses($startDate,$endDate,$msisdn,$serviceName){

    //     global $base_url;
    //     global $user_responses;
    //     return callWebApiGet($base_url.$user_responses.'/'.$startDate.'/'.$endDate.'/'.$msisdn.'/'.$serviceName);
    
    // }

?>
