<?php
    error_reporting(E_ALL);
    include_once('curl_url.php');
    $GOOGLE_API_KEY  = "AIzaSyDQTpHIDA-oUPXuJhXW7PHxRo_9OWUnoMs";
    $secret = '6LeUkr8ZAAAAAPLjUQfjU8takt8g7rxonze_GVIO';

    function getAuthToken(){
        global $base_url;
        $url = $base_url.'oauth/token';
        // $url = 'https://dev.vayaafrica.com/cx/oauth/token';
        $params = array(
            "scope" => 'read',
            "username" => 'admin',
            "password" => 'password',
            "client_id" => 'client_id',
            "client_secret" => 'client_secret',
            "redirect_uri" => 'http://' . $_SERVER["HTTP_HOST"] . $_SERVER["PHP_SELF"],
            "grant_type" => "password"
        );

        $ch = curl_init();
        curl_setopt($ch, constant("CURLOPT_" . 'URL'), $url);
        curl_setopt($ch, constant("CURLOPT_" . 'POST'), true);
        curl_setopt($ch, constant("CURLOPT_" . 'POSTFIELDS'), $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);

        if ($info['http_code'] === 200) {
            header('Content-Type: ' . $info['content_type']);

            $res_arr = json_decode($output,false);
            return $res_arr->access_token;
        } else {
            return 'An error happened';
        }
    }

  
    function callWebApiPost($jsonData, $curl_url){
        $ch = curl_init($curl_url);
        $jsonDataEncoded = json_encode($jsonData);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        $ACC_TOKEN = getAuthToken();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'authorization: Bearer '.$ACC_TOKEN));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);

        return $result;
    }

    function callWebApiDelete($curl_url){
        $ch = curl_init($curl_url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        $ACC_TOKEN = getAuthToken();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'authorization: Bearer '.$ACC_TOKEN));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);

        return $result;
    }

    function callWebApiGet($curl_url){
        $ch = curl_init($curl_url);
        $ACC_TOKEN = getAuthToken();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'authorization: Bearer '.$ACC_TOKEN));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }


 /* Start Questions */

    //get all survey questions
     function getAllSurveyQuestions(){
        global $base_url;
        global $get_survey_questions;

        return callWebApiGet($base_url.$get_survey_questions);
     }


function getAllSurveyQuestionsById($surveyId){
    global $base_url;
    global $get_all_questions_by_id;
    return callWebApiGet($base_url.$get_all_questions_by_id.$surveyId);

}

 /* End Questions */

  /* Start Responses */
        //save  responses

        function saveAllResponses($uuid,$serviceName,$questionIdOne,$responseOne,$additionalTextOne,$questionIdTwo,$responseTwo,$additionalTextTwo,$questionIdThree,$responseThree,$additionalTextThree,$questionIdFour,$responseFour,$additionalTextFour){
           
            $jsonData = array(
                'uuid' => $uuid,
                'serviceName' => $serviceName,
               'questionIdOne' => $questionIdOne,
                'responseOne' => $responseOne,
                'additionalTextOne' => $additionalTextOne,
                'questionIdTwo' => $questionIdTwo,
                'responseTwo' => $responseTwo,
                'additionalTextTwo' => $additionalTextTwo,
                'questionIdThree' => $questionIdThree,
                'responseThree' => $responseThree,
                'additionalTextThree' => $additionalTextThree,
                'questionIdFour' => $questionIdFour,
                'responseFour' => $responseFour,
                'additionalTextFour' => $additionalTextFour
           
            );

            global $base_url;
            global $save_responses;
            return callWebApiPost($jsonData,$base_url.$save_responses);

    }

/* End Responses  */

function adminLogin($email,$password)
{
  $jsonData = array(
    'email' => $email,
    'password' => $password
  );
  global $base_url;
  global $admin_login;
  return callWebApiPost($jsonData,$base_url.$admin_login);
}


function getAllQuestions(){
    global $base_url;
    global $get_all_questions;

    return callWebApiGet($base_url.$get_all_questions);
 }

 function getAllSurvey(){
    global $base_url;
    global $all_surveys;

    return callWebApiGet($base_url.$all_surveys);
 }
 function getAllServices(){
    global $base_url;
    global $all_services;

    return callWebApiGet($base_url.$all_services);
 }

     function addSurvey($surveyName,$status){

        $jsonData = array(
            'surveyName' => $surveyName,
            'status' => $status
        );

        global $base_url;
        global $create_survey;
        return callWebApiPost($jsonData,$base_url.$create_survey);

    }


    function addService($serviceName,$serviceStatus,$createdBy){

        $jsonData = array(
            'serviceName' => $serviceName,
            'serviceStatus' => $serviceStatus,
            'createdBy' => $createdBy
        );

        global $base_url;
        global $create_service;
        return callWebApiPost($jsonData,$base_url.$create_service);

    }

    function getAllConfigs(){
        global $base_url;
        global $all_configs;
    
        return callWebApiGet($base_url.$all_configs);
     }

     function getAllUsers(){
        global $base_url;
        global $all_users;
    
        return callWebApiGet($base_url.$all_users);
     }

     function getAllLimits(){
        global $base_url;
        global $all_limits;
    
        return callWebApiGet($base_url.$all_limits);
     }
     function addLimit($configId,$rate,$perNumberOfDays,$lastModifiedBy,$createdBy){

        $jsonData = array(
            'configId' => $configId,
            'rate' => $rate,
            'perNumberOfDays' => $perNumberOfDays,
            'lastModifiedBy' => $lastModifiedBy,
            'createdBy' => $createdBy,
            
            
        );

        global $base_url;
        global $create_limit;
        return callWebApiPost($jsonData,$base_url.$create_limit);

    }

//delete limit       
function deleteLimit($configId){          
    global $base_url;           
    global $delete_limit;            
    return callWebApiDelete($base_url.$delete_limit.$configId);       
    }


     function addConfig($name,$value){

        $jsonData = array(
            'name' => $name,
            'value' => $value
        );

        global $base_url;
        global $create_config;
        return callWebApiPost($jsonData,$base_url.$create_config);

    }

    function addUser($userName,$firstName,$lastName,$password,$email,$mobileNumber,$accessType){

        $jsonData = array(
            'userName' => $userName,
            'firstName' => $firstName,
            'lastName' => $lastName,
            'password' => $password,
            'email' => $email,
            'mobileNumber' => $mobileNumber,
            'accessType' => $accessType
        );

        global $base_url;
        global $create_user;
        return callWebApiPost($jsonData,$base_url.$create_user);

    }
    function forgotPassword($email){

        $jsonData = array(
            
            'email' => $email
        
        );

        global $base_url;
        global $forgot_password;
        return callWebApiPost($jsonData,$base_url.$forgot_password);

    }
    function resetPassword($email,$resetToken,$newpassword){

        $jsonData = array(
            
            'email' => $email,
            'resetToken' => $resetToken,
            'newpassword' => $newpassword
        
        );

        global $base_url;
        global $reset_password;
        return callWebApiPost($jsonData,$base_url.$reset_password);

    }


    function createQuestion($surveyId,$questionNumber,$question,$createdBy){

        $jsonData = array(
            'surveyId' => $surveyId,
            'questionNumber' => $questionNumber,
            'question' => $question,
            'createdBy' => $createdBy
        );
        global $base_url;
        global $create_question;
        return callWebApiPost($jsonData,$base_url.$create_question);

    }

    function editSurvey($surveyId,$surveyName,$status,$lastModifiedBy){
        
        $jsonData = array(
            'surveyId' => $surveyId,
            'surveyName' => $surveyName,
            'status' => $status,
            'lastModifiedBy' => $lastModifiedBy
        );
        global $base_url;
        global $update_survey;
        return callWebApiPost($jsonData,$base_url.$update_survey);

    }
    
    
function editQuestion($questionId,$surveyId,$questionNumber,$question,$lastModifiedBy){
        
    $jsonData = array(
        'questionId'=>$questionId,
        'surveyId' => $surveyId,
        'questionNumber' => $questionNumber,
        'question' => $question,
        'lastModifiedBy' => $lastModifiedBy
    );
    global $base_url;
    global $update_question;
    return callWebApiPost($jsonData,$base_url.$update_question);

}
    function editUser($userId,$userName,$firstName,$lastName,$accessType,$email,$mobileNumber,$lastModifiedBy){
        
        $jsonData = array(
            'userId' => $userId,
            'userName' => $userName,
            'firstName' => $firstName,
            'lastName' => $lastName,
            'accessType' => $accessType,       
            'email' => $email,
            'mobileNumber' => $mobileNumber,
            'lastModifiedBy' => $lastModifiedBy
           
        );
        global $base_url;
        global $update_user;
        return callWebApiPost($jsonData,$base_url.$update_user);

    }

    function editService($serviceId,$serviceName,$status,$ratingLimit,$lastModifiedBy){

        $jsonData = array(
            'serviceId' => $serviceId,
            'serviceName' => $serviceName,
            'status' => $status,
            'ratingLimit' => $ratingLimit,
            'lastModifiedBy' => $lastModifiedBy
        );
        global $base_url;
        global $update_service;
        return callWebApiPost($jsonData,$base_url.$update_service);

    }


    function  editConfig($configId,$name,$value,$lastModifiedBy){

        $jsonData = array(
            'configId' => $configId,
            'name' => $name,
            'value' => $value,
            'lastModifiedBy' => $lastModifiedBy
        );
        global $base_url;
        global $update_config;
        return callWebApiPost($jsonData,$base_url.$update_config);

    }

    function getAllResponses($startDate,$endDate){

        global $base_url;
        global $user_responses;
        return callWebApiGet($base_url.$user_responses.'/'.$startDate.'/'.$endDate);
    
    }

   // use this to search by startDate,endDate,msisdn,serviceName
    // function getAllResponses($startDate,$endDate,$msisdn,$serviceName){

    //     global $base_url;
    //     global $user_responses;
    //     return callWebApiGet($base_url.$user_responses.'/'.$startDate.'/'.$endDate.'/'.$msisdn.'/'.$serviceName);
    
    // }

?>
