<?php
   session_start();
    include_once('../utils/EcoCashHoldingsCxUtility.php');
    if(isset($_POST['login']))
    {
        if(!empty($_POST['email']) && !empty($_POST['password']) && !empty($_POST['userName']) && !empty($_POST['firstName']) && !empty($_POST['lastName']) && !empty($_POST['mobileNumber']) && !empty($_POST['accessType']))
        {
            $userName = trim($_POST['userName']);
            $firstName = trim($_POST['firstName']);
            $lastName = trim($_POST['lastName']);
            $password = trim($_POST['password']);
            $email = trim($_POST['email']);
            $mobileNumber = trim($_POST['mobileNumber']);
            $accessType = trim($_POST['accessType']);

            $createUser = json_decode(addUser($userName,$firstName,$lastName,$password,$email,$mobileNumber,$accessType),true);
            var_dump($createUser);
            exit;

            if($createUser['responseStatus'] == "SUCCESS"){
                $errorMsg = "User created successfully";
                header('Refresh: 2; URL = admin-login');
                exit;
            } else {
            session_destroy();
            $errorMsg = $createUser['responseMessage'];
            }
            if(isset($_GET['logout']) && $_GET['logout'] == true)
            {
            session_destroy();
            header("location: portal/admin-login");
            exit;
            }

            if(isset($_GET['lmsg']) && $_GET['lmsg'] == true)
            {
                $errorMsg = "Login required to access dashboard";
            }
        }
    }

?>

<!DOCTYPE html>
<html lang="zxx" class="js">

<head>
    <base href="../">
    <meta charset="utf-8">
    <meta name="author" content="Softnio">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="clean city.">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="./images/favicon.png">
    <!-- Page Title  -->
    <title>EcoCash Holdings - Leading PAN Africa Technology solutions group</title>
    <!-- StyleSheets  -->
    <link rel="stylesheet" href="./assets/css/dashlite.css?ver=2.9.0">
    <link id="skin-default" rel="stylesheet" href="./assets/css/theme.css?ver=2.9.0">
    <link rel="stylesheet" href="./assets/validate-password-requirements/css/jquery.passwordRequirements.css" />
    <style>
        .btn-primary {
    color: #fff;
    background-color: #888888;
    border-color: #888888;

    
    
}

.passwordInput{          
       margin-top: 5%;           
      /* text-align :center; */   
         }     
         .displayBadge{ 
         margin-top: 5%; 
         display: none;            
     text-align :center;      
         }

body {
    background: url("./images/stock/eco_image.jpg") no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
}

    </style>
</head>

<body class="nk-body bg-white npc-general pg-auth">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <!-- wrap @s -->
            <div class="nk-wrap nk-wrap-nosidebar">
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="nk-block nk-block-middle nk-auth-body  wide-xs">
                        <div class="brand-logo pb-4 text-center">
                            <a href="html/index.html" class="logo-link">
                                                     </a>
                        </div>
                        <div class="card card-bordered">
                            <div class="card-inner card-inner-lg">
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                    <!-- <img class="logo-light logo-img logo-img-md" src="./images/ecocash_logo.png" srcset="./images/ecocash_logo.png" alt="logo"> -->
                                    <img class="logo-dark logo-img logo-img-md "src="./images/ecocash_logo.png" srcset="./images/ecocash_logo.png" alt="logo-dark">
                              
                                    </div>
                                </div>
                                <form action="portal/xyzs" method="post">
                                <div class="row g-gs">
                        <div class="col-md-6">
                            <div class="form-group">
                                    <label class="form-label" for="payment-name-add">Username</label>
                                    <input type="text" class="form-control"  name ="userName" placeholder="">
                                </div>
                            </div>
                            <div class="col-md-6">
                            <div class="form-group">
                                    <label class="form-label" for="payment-name-add">First Name</label>
                                    <input type="text" class="form-control"  name ="firstName" placeholder="">
                                </div>
                            </div>
                            <div class="col-md-6">
                            <div class="form-group">
                                    <label class="form-label" for="payment-name-add">Last Name</label>
                                    <input type="text" class="form-control"  name ="lastName" placeholder="">
                                </div>
                            </div>
                            <div class="col-md-6">
                            <div class="form-group">
                                    <label class="form-label" for="payment-name-add">Email</label>
                                    <input type="email" class="form-control"  name ="email" placeholder="">
                                </div>
                            </div>
                            <div class="col-md-6">
                            <div class="form-group">
                                    <label class="form-label" for="payment-name-add">Mobile Number</label>
                                    <input type="text" class="form-control"  name ="mobileNumber" placeholder="">
                                </div>
                            </div>
                            <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">Access Type</label>
                            <div class="form-control-wrap">
                                <select name="accessType" class="form-select" data-placeholder="Select Access Type" >
                                <option value="ADMIN">IS ADMIN</option>
                                <option value="SUPERADMIN">CX ADMIN</option>
                                </select>
                            </div>
                        </div>
                        </div>
                            <div class="col-md-6">
                            <div class="form-group">
                                    <label class="form-label" for="payment-name-add">Password</label>
                                    <input type="password" class="form-control passwordInput" name="password" id="PassEntry" required>
                            <span id="StrengthDisp" class="badge displayBadge">Weak</span>
                                </div>
                            </div>
                                </div>
                                <br/>
                                    <div class="form-group">
                                    <button type="submit"  class="btn btn-lg btn-primary btn-block" name="login" >Register</button>
                                    </div>
                                </form>
                                <!-- <div class="form-note-s2 text-center pt-4"> New on our platform? <a href="portal/register">Create an account</a> -->
                                </div>
                                <!-- <div class="text-center pt-4 pb-3">
                                    <h6 class="overline-title overline-title-sap"><span>OR</span></h6>
                                </div>
                                <ul class="nav justify-center gx-4">
                                    <li class="nav-item"><a class="nav-link" href="#">Facebook</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#">Google</a></li>
                                </ul> -->
                            </div>
                        </div>
                    </div>
                    <div class="nk-footer nk-auth-footer-full">
                        <div class="container wide-lg">
                            <!-- <div class="row g-3">
                                <div class="col-lg-6 order-lg-last">
                                    <ul class="nav nav-sm justify-content-center justify-content-lg-end">
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">Terms & Condition</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">Privacy Policy</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">Help</a>
                                        </li>
                                   
                                    </ul>
                                </div> -->
                                <div class="col-lg-6">
                                    <div class="nk-block-content text-center text-lg-left">
                                    <p> &copy; Copyright Ecocash Holdings Zimbabwe 2022 <a href="https://www.ecocashholdings.co.zw/" target="_blank"></a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- wrap @e -->
            </div>
            <!-- content @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->
    <!-- JavaScript -->
    <script src="./assets/js/bundle.js"></script>
    <script src="./assets/js/scripts.js"></script>
    <script src="./assets/validate-password-requirements/js/jquery.passwordRequirements.min.js"></script>
    
    <script>

let timeout;

    // traversing the DOM and getting the input and span using their IDs
let password = document.getElementById('PassEntry');
    let strengthBadge = document.getElementById('StrengthDisp');

    // The strong and weak password Regex pattern checker
let strongPassword = new RegExp('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{8,})');
    let mediumPassword = new RegExp('((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{6,}))|((?=.*[a-z])(?=.*[A-Z])(?=.*[^A-Za-z0-9])(?=.{8,}))');
    
    function StrengthChecker(PasswordParameter){
        // We then change the badge's color and text based on the password strength
if(strongPassword.test(PasswordParameter)) {
            strengthBadge.style.backgroundColor = 'green';
            strengthBadge.textContent = 'Strong';
        } else if(mediumPassword.test(PasswordParameter)){
            strengthBadge.style.backgroundColor = 'blue';
            strengthBadge.textContent = 'Medium';
        } else{
            strengthBadge.style.backgroundColor = 'red';
            strengthBadge.textContent = 'Weak';
        }
    }

password.addEventListener("input", () => {
    strengthBadge.style.display = 'block';
    clearTimeout(timeout);
    timeout = setTimeout(() => StrengthChecker(password.value), 500);
    if(password.value.length !== 0) {
        strengthBadge.style.display != 'block';
    } else {
        strengthBadge.style.display = 'none';
    }
});


</script>
    <!-- Google reCaptcha -->
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <!-- select region modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="region">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                <div class="modal-body modal-body-md">
                    <h5 class="title mb-4">Select Your Country</h5>
                    <div class="nk-country-region">
                        <ul class="country-list text-center gy-2">
                            <li>
                                <a href="#" class="country-item">
                                    <img src="./images/flags/arg.png" alt="" class="country-flag">
                                    <span class="country-name">Argentina</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="country-item">
                                    <img src="./images/flags/aus.png" alt="" class="country-flag">
                                    <span class="country-name">Australia</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="country-item">
                                    <img src="./images/flags/bangladesh.png" alt="" class="country-flag">
                                    <span class="country-name">Bangladesh</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="country-item">
                                    <img src="./images/flags/canada.png" alt="" class="country-flag">
                                    <span class="country-name">Canada <small>(English)</small></span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="country-item">
                                    <img src="./images/flags/china.png" alt="" class="country-flag">
                                    <span class="country-name">Centrafricaine</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="country-item">
                                    <img src="./images/flags/china.png" alt="" class="country-flag">
                                    <span class="country-name">China</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="country-item">
                                    <img src="./images/flags/french.png" alt="" class="country-flag">
                                    <span class="country-name">France</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="country-item">
                                    <img src="./images/flags/germany.png" alt="" class="country-flag">
                                    <span class="country-name">Germany</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="country-item">
                                    <img src="./images/flags/iran.png" alt="" class="country-flag">
                                    <span class="country-name">Iran</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="country-item">
                                    <img src="./images/flags/italy.png" alt="" class="country-flag">
                                    <span class="country-name">Italy</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="country-item">
                                    <img src="./images/flags/mexico.png" alt="" class="country-flag">
                                    <span class="country-name">México</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="country-item">
                                    <img src="./images/flags/philipine.png" alt="" class="country-flag">
                                    <span class="country-name">Philippines</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="country-item">
                                    <img src="./images/flags/portugal.png" alt="" class="country-flag">
                                    <span class="country-name">Portugal</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="country-item">
                                    <img src="./images/flags/s-africa.png" alt="" class="country-flag">
                                    <span class="country-name">South Africa</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="country-item">
                                    <img src="./images/flags/spanish.png" alt="" class="country-flag">
                                    <span class="country-name">Spain</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="country-item">
                                    <img src="./images/flags/switzerland.png" alt="" class="country-flag">
                                    <span class="country-name">Switzerland</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="country-item">
                                    <img src="./images/flags/uk.png" alt="" class="country-flag">
                                    <span class="country-name">United Kingdom</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="country-item">
                                    <img src="./images/flags/english.png" alt="" class="country-flag">
                                    <span class="country-name">United State</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div><!-- .modal-content -->
        </div><!-- .modla-dialog -->
    </div><!-- .modal -->

</html>