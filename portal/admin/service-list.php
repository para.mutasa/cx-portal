<?php
session_start();
if (!isset($_SESSION['access_type'])) {
    header("Location: ../../portal/admin-login");
    exit();
}
$admin_id =  $_SESSION["admin_id"];
$admin_phone  = $_SESSION["admin_vphone"];
$admin_email =  $_SESSION["admin_Email"];
$admin_firstname =  $_SESSION["first_name"];
$admin_lastname = $_SESSION["last_name"];

include_once('../../utils/EcoCashHoldingsCxUtility.php');
require_once('includes/header.php');
$services = json_decode(getAllServices(), true);
// var_dump($services);
// exit;
// $configs= json_decode(getAllConfigs(), true);
$limits = json_decode(getAllLimits(), true);

// var_dump($services);
// exit;
?>

<body class="nk-body bg-lighter npc-general has-sidebar ">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <!-- sidebar @s -->
            <?php require_once('includes/sidebar.php'); ?>
            <!-- sidebar @e -->


            <!-- wrap @s -->
            <div class="nk-wrap ">
                <!-- main header @s -->
                <div class="nk-header nk-header-fixed is-light">
                    <div class="container-fluid">
                        <div class="nk-header-wrap">
                            <div class="nk-menu-trigger d-xl-none ml-n1">
                                <a href="#" class="nk-nav-toggle nk-quick-nav-icon" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
                            </div>
                            <div class="nk-header-brand d-xl-none">
                                <a href="portal/admin/index" class="logo-link">
                                    <img class="logo-light logo-img" src="./images/logo.png" srcset="./images/logo2x.png 2x" alt="logo">
                                    <img class="logo-dark logo-img" src="./images/logo-dark.png" srcset="./images/logo-dark2x.png 2x" alt="logo-dark">
                                </a>
                            </div><!-- .nk-header-brand -->
                            <div class="nk-header-news d-none d-xl-block">
                                <div class="nk-news-list">
                                    <a class="nk-news-item" href="#">
                                        <!-- <div class="nk-news-icon">
                                            <em class="icon ni ni-card-view"></em>
                                        </div>
                                        <div class="nk-news-text">
                                            <p>Do you know the latest update of 2021? <span> A overview of our is now available on YouTube</span></p>
                                            <em class="icon ni ni-external"></em>
                                        </div> -->
                                    </a>
                                </div>
                            </div><!-- .nk-header-news -->
                            <div class="nk-header-tools">
                                <ul class="nk-quick-nav">
                                    <li class="dropdown user-dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <div class="user-toggle">
                                                <div class="user-avatar sm">
                                                    <em class="icon ni ni-user-alt"></em>
                                                </div>
                                                <div class="user-info d-none d-md-block">
                                                    <div class="user-status">Administrator</div>
                                                    <div class="user-name dropdown-indicator"><?php echo $admin_firstname; ?></div>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right dropdown-menu-s1">
                                            <div class="dropdown-inner user-card-wrap bg-lighter d-none d-md-block">
                                                <div class="user-card">
                                                    <div class="user-avatar">
                                                        <span>AB</span>
                                                    </div>
                                                    <div class="user-info">
                                                        <span class="lead-text"><?php echo  $admin_firstname; ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                    <!-- <li><a href="#"><em class="icon ni ni-user-alt"></em><span>View Profile</span></a></li>
                                                    <li><a href="html/hotel/settings.html"><em class="icon ni ni-setting-alt"></em><span>Account Setting</span></a></li>
                                                    <li><a href="html/hotel/settings-activity-log.html"><em class="icon ni ni-activity-alt"></em><span>Login Activity</span></a></li> -->
                                                    <li><a class="dark-switch" href="#"><em class="icon ni ni-moon"></em><span>Dark Mode</span></a></li>
                                                </ul>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                    <li><a href="portal/admin/logout"><em class="icon ni ni-signout"></em><span>Sign out</span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li><!-- .dropdown -->

                                </ul><!-- .nk-quick-nav -->
                            </div><!-- .nk-header-tools -->
                        </div><!-- .nk-header-wrap -->
                    </div><!-- .container-fliud -->
                </div>
                <!-- main header @e -->
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="container-fluid">
                        <div class="nk-content-inner">
                            <div class="nk-content-body">
                                <div class="nk-block-head nk-block-head-sm">
                                    <div class="nk-block-between">
                                        <div class="nk-block-head-content">
                                            <h3 class="nk-block-title page-title">Service List</h3>
                                            <div class="nk-block-des text-soft">
                                                <!-- <p>You have total 1 Currency.</p> -->
                                            </div>
                                        </div><!-- .nk-block-head-content -->
                                        <div class="nk-block-head-content">
                                            <div class="toggle-wrap nk-block-tools-toggle">
                                                <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="pageMenu"><em class="icon ni ni-menu-alt-r"></em></a>
                                                <div class="toggle-expand-content" data-content="pageMenu">
                                                    <ul class="nk-block-tools g-3">
                                                        <li class="nk-block-tools-opt"><a href="#" data-toggle="modal" data-target="#add-service" class="btn btn-primary"><em class="icon ni ni-reports"></em><span>Add Service</span></a></li>

                                                    </ul>
                                                </div>
                                            </div><!-- .toggle-wrap -->
                                        </div><!-- .nk-block-head-content -->
                                    </div><!-- .nk-block-between -->
                                </div><!-- .nk-block-head -->

                                <div class="nk-block nk-block-lg">

                                    <div class="card card-preview">
                                        <div class="card-inner">
                                            <table class="datatable-init nowrap nk-tb-list nk-tb-ulist" data-order='[[0, "desc"]]' data-auto-responsive="false">
                                                <thead>
                                                    <tr class="nk-tb-item nk-tb-head">
                                                        <th class="nk-tb-col"><span class="sub-text">Name</span></th>
                                                        <th class="nk-tb-col"><span class="sub-text">Rating Limit</span></th>
                                                        <th class="nk-tb-col tb-col-md"><span class="sub-text">Status</span></th>
                                                        <th class="nk-tb-col nk-tb-col-tools text-right">
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php if ($services['responseStatus'] == "SUCCESS") : ?>
                                                        <?php
                                                        foreach ($services['eobjResponse'] as $service) :
                                                        ?>
                                                            <tr>
                                                                <td class="nk-tb-col tb-col-md"><span><?= $service['serviceName'] ?></span></td>
                                                                <td class="nk-tb-col tb-col-md"><span><?= $service['rate'] ?></span></td>

                                                                <td class="nk-tb-col tb-col-md"><span><?= $service['status'] ?></span></td>

                                                                <td class="nk-tb-col nk-tb-col-tools">
                                                                    <ul class="nk-tb-actions gx-1">

                                                                        <li>
                                                                            <div class="drodown">
                                                                                <a href="#" class="dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
                                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                                    <ul class="link-list-opt no-bdr">
                                                                                        <li><a href="#" data-toggle="modal" data-target="#edit-service<?= $service['serviceId'] ?>"><em class="icon ni ni-edit"></em><span>Edit Service</span></a></li>
                                                                                        <li><a href="#" data-toggle="modal" data-target="#cred-service<?= $service['serviceId'] ?>"><em class="icon ni ni-edit"></em><span>Service Credentials</span></a></li>
                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                    </ul>
                                                                </td>
                                                                <!-- update Service-->
                                                                <div class="modal fade" tabindex="-1" role="dialog" id="edit-service<?= $service['serviceId'] ?>">
                                                                    <div class="modal-dialog modal-md" role="document">
                                                                        <div class="modal-content">
                                                                            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                                                                            <div class="modal-body modal-body-lg">
                                                                                <div class="gy-4">
                                                                                    <div class="example-alert">
                                                                                        <div class="alert alert-pro alert-primary">
                                                                                            <div class="alert-text">
                                                                                                <h5 class="modal-title">Edit Service</h5>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <form id="edit-service<?= $service['serviceId'] ?>" class="mt-2">
                                                                                    <div class="row g-gs">
                                                                                        <div class="col-md-10">
                                                                                            <div class="form-group">
                                                                                                <label class="form-label" for="payment-name-add">Name</label>
                                                                                                <input type="text" class="form-control" name="serviceName" value="<?= $service['serviceName'] ?>">
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="col-md-10">
                                                                                            <div class="form-group">
                                                                                                <label class="form-label">Status</label>
                                                                                                <div class="form-control-wrap">
                                                                                                    <select name="status" class="form-select" data-placeholder="Select Status">
                                                                                                        <option value="ENABLED">ENABLED</option>
                                                                                                        <option value="DISABLED">DISABLED</option>
                                                                                                        <option value="DELETED">DELETED</option>
                                                                                                    </select>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="col-md-6">
                                                                                            <div class="form-group">
                                                                                                <label class="form-label">Rating Limit</label>
                                                                                                <div class="form-control-wrap">
                                                                                                    <select class="form-control" name="ratingLimit" required>
                                                                                                        <?php foreach ($limits['eobjResponse']  as $limit) : ?>
                                                                                                            <option value="<?= $limit["configId"] ?>"><?= $limit["rate"] . " " . "per" . " " . ($limit["perNumberOfDays"]) . "day/s" . "" ?></option>
                                                                                                        <?php endforeach; ?>
                                                                                                    </select>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <input type="hidden" class="form-control" name="lastModifiedBy" value="<?php echo $admin_email; ?>">
                                                                                        <input type="hidden" class="form-control" name="serviceId" value="<?= $service['serviceId'] ?>">
                                                                                        <div class="col-12">
                                                                                            <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                                                                                <li>
                                                                                                    <input type="hidden" name="edit_service" value="true">
                                                                                                    <button type="button" class="btn btn-primary" name="edit_service" onClick="editService('<?= $service["serviceId"] ?>')">Submit</button>
                                                                                                </li>

                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </form>
                                                                            </div><!-- .modal-body -->
                                                                            <div class="modal-footer bg-light">
                                                                                <span class="sub-text"></span>
                                                                            </div>
                                                                        </div><!-- .modal-content -->
                                                                    </div><!-- .modal-dialog -->
                                                                </div><!-- .modal -->
                                                                <!-- Generate  Service Credentials-->
                                                                <div class="modal fade" tabindex="-1" role="dialog" id="cred-service<?= $service['serviceId'] ?>">
                                                                    <div class="modal-dialog modal-sm" role="document">
                                                                        <div class="modal-content">
                                                                            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                                                                            <div class="modal-body modal-body-lg">
                                                                                <div class="gy-4">
                                                                                    <div class="example-alert">
                                                                                        <div class="alert alert-pro alert-primary">
                                                                                            <div class="alert-text">
                                                                                                <h5 class="modal-title">Service Credentials</h5>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <form id="cred-service<?= $service['serviceId'] ?>" class="mt-2">
                                                                                    <div class="row g-gs">
                                                                                        <div class="col-md-10">
                                                                                            <div class="form-group">
                                                                                                <label class="form-label" for="payment-name-add">Service Name</label>
                                                                                                <input type="text" class="form-control" name="serviceName" value="<?= $service['serviceName'] ?>" readonly>
                                                                                            </div>
                                                                                        </div>

                                                                                        <input type="hidden" class="form-control" name="lastModifiedBy" value="<?php echo $admin_email; ?>">
                                                                                        <input type="hidden" class="form-control" name="serviceId" value="<?= $service['serviceId'] ?>">
                                                                                        <div class="col-12">
                                                                                            <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                                                                                <li>
                                                                                                    <input type="hidden" name="cred_service" value="true">
                                                                                                    <button type="button" class="btn btn-primary" name="cred_service" onClick="credService('<?= $service["serviceId"] ?>')">Generate</button>
                                                                                                </li>

                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </form>
                                                                            </div><!-- .modal-body -->
                                                                            <div class="modal-footer bg-light">
                                                                                <span class="sub-text"></span>
                                                                            </div>
                                                                        </div><!-- .modal-content -->
                                                                    </div><!-- .modal-dialog -->
                                                                </div><!-- .modal -->


                                                            <?php endforeach; ?>

                                                        <?php else : ?>

                                                            <?= $services['responseMessage'] ?>

                                                        <?php endif; ?>


                                                </tbody>
                                            </table>
                                        </div>
                                    </div><!-- .card-preview -->
                                </div> <!-- nk-block -->


                            </div>
                        </div>
                    </div>
                </div>
                <!-- content @e -->
                <!-- footer @s -->
                <div class="nk-footer">
                    <div class="container-fluid">
                        <div class="nk-footer-wrap">
                            <div class="nk-footer-copyright"> &copy; Copyright Ecocash Holdings Zimbabwe 2022 <a href="https://www.ecocashholdings.co.zw/" target="_blank"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- footer @e -->
            </div>
            <!-- wrap @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->

    <!-- Success Send Cred Modal Alert -->
    <div class="modal fade" tabindex="-1" id="successSendAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                            <div class="caption-text">You’ve successfully send token URL</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Fail  Modal Alert -->
    <div class="modal fade" tabindex="-1" id="failSendAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                            <div class='sendCredResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Success Modal Alert -->
    <div class="modal fade" tabindex="-1" id="successAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                            <div class="caption-text">You’ve successfully added service</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Fail  Modal Alert -->
    <div class="modal fade" tabindex="-1" id="failAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                            <div class='addServiceResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Success Modal Alert -->
    <div class="modal fade" tabindex="-1" id="successAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                            <div class="caption-text">You’ve successfully added service</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Fail  Modal Alert -->
    <div class="modal fade" tabindex="-1" id="failAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                            <div class='addServiceResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Success Edit Modal Alert -->
    <div class="modal fade" tabindex="-1" id="successServiceAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                            <div class="caption-text">You’ve successfully updated service</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Fail  Edit Modal Alert -->
    <div class="modal fade" tabindex="-1" id="failServiceEditAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                            <div class='updateServiceResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--Fail Cred Modal Alert -->
    <div class="modal fade" tabindex="-1" id="failCredAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                            <div class='credServiceResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Fail Cred Modal Alert -->
    <div class="modal fade" tabindex="-1" id="showSpinnerAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <div class="nk-modal-text">
                            <div class="d-flex justify-content-center">
                                <div class="spinner-border" id="spinner" style="width: 4rem; height: 4rem; margin-right: auto; margin-left: 160px;" role="status"><span class="sr-only">Loading...</span></div>
                            </div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Add Service-->
    <div class="modal fade" tabindex="-1" role="dialog" id="add-service">
        <div class="modal-dialog modal-mb" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                <div class="modal-body modal-body-md">
                    <div class="gy-4">
                        <div class="example-alert">
                            <div class="alert alert-pro alert-primary">
                                <div class="alert-text">
                                    <h5 class="modal-title">Add Service</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="d-flex justify-content-center">  <div class="spinner-border" id="spinner" style="width: 4rem; height: 4rem;" role="status">    <span class="sr-only">Loading...</span>  </div></div> -->
                    <form id="addservice" action="#" class="mt-2">
                        <div class="row g-gs">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label class="form-label" for="payment-name-add">Name</label>
                                    <input type="text" class="form-control" name="serviceName" placeholder="">
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="form-group">
                                    <label class="form-label">Status</label>
                                    <div class="form-control-wrap">
                                        <select name="status" class="form-select" data-placeholder="Select Status">
                                            <option value="ENABLED">ENABLED</option>
                                            <option value="DISABLED">DISABLED</option>
                                            
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" class="form-control" name="createdBy" value="<?php echo $admin_email; ?>">
                            <div class="col-10">
                                <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                    <li>
                                        <input type="hidden" class="form-control" name="add_service">
                                        <button type="button" class="btn btn-primary" name="add_service" onClick="addService()">Submit</button>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </form>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-light">
                    <span class="sub-text"></span>
                </div>
            </div><!-- .modal-content -->
        </div><!-- .modal-dialog -->
    </div><!-- .modal -->


    <div class="modal fade" tabindex="-1" role="dialog" id="view-cred-service">
        <div class="modal-dialog modal-mb" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                <div class="modal-body modal-body-md">
                    <div class="gy-4">
                        <div class="example-alert">
                            <div class="alert alert-pro alert-primary">
                                <div class="alert-text">
                                    <h5 class="modal-title">Send Service Token URL</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form id="sendcred" action="#" class="mt-2">
                        <div class="row g-gs">
                            <!-- <div class="col-md-6">
                            <div class="form-group">
                            <label class="form-label" for="payment-name-add">System ID</label>
                            <input type="text" id ="userName" class="form-control" readonly>
                                </div>
                            </div> -->
                            <div class="col-md-12">
                                <div class="form-control-wrap">
                                    <label class="form-label" for="payment-name-add">System ID</label>
                                    <div class="input-group">
                                        <input type="text" id="userName" name="serviceName" class="form-control" readonly>
                                        <div class="input-group-append">
                                            <button type="button" onclick="copySystemID()" class="btn btn-outline-primary btn-dim">Copy</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="col-md-6">
                            <div class="form-group">
                            <label class="form-label" for="payment-name-add">Password</label>
                            <input type="text" id ="password" class="form-control" readonly>
                                </div>
                            </div> -->
                            <!-- <div class="col-md-12">
                            <div class="form-group">
                            <label class="form-label" for="payment-name-add">Token URL</label>
                            <input type="text" id ="url" class="form-control" readonly>
                                </div>
                            </div> -->
                            <div class="col-md-12">
                                <div class="form-control-wrap">
                                    <label class="form-label" for="payment-name-add">Token URL</label>
                                    <div class="input-group">
                                        <input type="text" id="url" class="form-control" readonly>
                                        <div class="input-group-append">
                                            <button type="button" onclick="copyTokenURL()" class="btn btn-outline-primary btn-dim">Copy</button>
                                        </div>
                                        <br />
                                        <!-- <p id="demo"></p> -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-control-wrap">
                                    <label class="form-label" for="payment-name-add">Recipient Email</label>
                                    <div class="input-group">
                                        <input type="email" name="email" class="form-control">
                                        <div class="input-group-append">
                                            <input type="hidden" class="form-control" name="send_cred">
                                            <button type="button" onClick="sendCred()" name="send_cred" class="btn btn-outline-primary btn-dim">Send</button>
                                        </div>
                                        <br />
                                        <!-- <p id="demo"></p> -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-10">
                            </div>
                        </div>
                    </form>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-light">
                    <span class="sub-text"></span>
                </div>
            </div><!-- .modal-content -->
        </div><!-- .modal-dialog -->
    </div><!-- .modal -->

    <script>
        function copyTokenURL() {
            /* Get the text field */
            var copyText = document.getElementById("url");

            /* Select the text field */
            copyText.select();
            copyText.setSelectionRange(0, 99999); /* For mobile devices */

            /* Copy the text inside the text field */
            navigator.clipboard.writeText(copyText.value);

            /* Alert the copied text */
            document.getElementById("demo").innerHTML = "Copied";
        }
    </script>

    <script>
        function copySystemID() {
            /* Get the text field */
            var copyText = document.getElementById("userName");

            /* Select the text field */
            copyText.select();
            copyText.setSelectionRange(0, 99999); /* For mobile devices */

            /* Copy the text inside the text field */
            navigator.clipboard.writeText(copyText.value);

            /* Alert the copied text */
            document.getElementById("demo").innerHTML = "Copied";
        }
    </script>

    <!-- BEGIN: AJAX CALLS-->

    <script>
        //Send Cred
        function sendCred() {
            $.ajax({
                type: "POST",
                url: "portal/survey/controller/process.php",
                data: $('form#sendcred').serialize(),
                cache: false,
                success: function(response) {
                    var json = $.parseJSON(response);
                    if (json.responseStatus == "SUCCESS") {
                        $('#view-cred-service').modal('hide');
                        $("#successSendAlert").modal('show');
                        setTimeout(function() {
                            window.location = "portal/admin/service-list";
                        }, 2000);
                    } else {
                        $('.sendCredResponse').empty();
                        $('.sendCredResponse').html(json.responseMessage);
                        $('#view-cred-service').modal('hide');
                        $("#failSendAlert").modal('show');
                        $("#failSendAlert").on("hidden.bs.modal", function() {
                            $(".sendCredResponse").html("");
                        });

                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('.sendCredResponse').empty();
                    $('.sendCredResponse').append(errorThrown);
                    $('#view-cred-service').modal('hide');
                    $("#failSendAlert").modal('show');
                    $("#failSendAlert").on("hidden.bs.modal", function() {
                        $(".sendCredResponse").html("");
                    });
                }
            });
        }
    </script>
    <script>
        //Add Service
        function addService() {
            $.ajax({
                type: "POST",
                url: "portal/survey/controller/process.php",
                data: $('form#addservice').serialize(),
                cache: false,
                success: function(response) {
                    var json = $.parseJSON(response);
                    if (json.responseStatus == "SUCCESS") {
                        $('#add-service').modal('hide');
                        $("#successAlert").modal('show');
                        setTimeout(function() {
                            window.location = "portal/admin/service-list";
                        }, 2000);
                    } else {
                        $('.addServiceResponse').empty();
                        $('.addServiceResponse').html(json.responseMessage);
                        $('#add-service').modal('hide');
                        $("#failAlert").modal('show');
                        $("#failAlert").on("hidden.bs.modal", function() {
                            $(".addServiceResponse").html("");
                        });

                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('.addServiceResponse').empty();
                    $('.addServiceResponse').append(errorThrown);
                    $('#add-service').modal('hide');
                    $("#failAlert").modal('show');
                    $("#failAlert").on("hidden.bs.modal", function() {
                        $(".addServiceResponse").html("");
                    });
                }
            });
        }
    </script>

    <script>
        //edit service
        function editService(serviceId) {
            $.ajax({
                type: "POST",
                url: "portal/survey/controller/process.php",
                data: $('form#edit-service' + serviceId).serialize(),
                cache: false,
                success: function(response) {
                    var json = $.parseJSON(response);
                    if (json.responseStatus == "SUCCESS") {
                        $('#edit-service' + serviceId).modal('hide');
                        $("#successServiceAlert").modal('show');
                        setTimeout(function() {
                            window.location = "portal/admin/service-list";
                        }, 2000);

                    } else {
                        $('.updateServiceResponse').empty();
                        $('.updateServiceResponse').append(json.responseMessage);
                        $('#edit-service' + serviceId).modal('hide');
                        $("#failServiceEditAlert").modal('show');
                        $("#failServiceEditAlert").on("hidden.bs.modal", function() {
                            $(".updateServiceResponse").html("");
                        });
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('.updateServiceResponse').empty();
                    $('.updateServiceResponse').append(errorThrown);
                    $('#edit-service' + serviceId).modal('hide');
                    $("#failServiceEditAlert").modal('show');
                    $("#failServiceEditAlert").on("hidden.bs.modal", function() {
                        $(".updateServiceResponse").html("");
                    });
                }
            });
        }
    </script>

    <script>
        //cred service
        function credService(serviceId) {
            $("#showSpinnerAlert").modal('show');
            $('#spinner').show();
            $('#cred-service' + serviceId).modal('hide');
            $.ajax({
                type: "POST",
                url: "portal/survey/controller/process.php",
                data: $('form#cred-service' + serviceId).serialize(),
                cache: false,
                success: function(response) {
                    var json = $.parseJSON(response);
                    console.log(json);
                    console.log(json.eobjResponse.password);
                    console.log(json.eobjResponse.userName);
                    if (json.responseStatus == "SUCCESS") {
                        $("#showSpinnerAlert").modal('hide');
                        $('#spinner').hide();
                        $('#cred-service' + serviceId).modal('hide');
                        $("#userName").val(json.eobjResponse.userName);
                        $("#password").val(json.eobjResponse.password);
                        $("#url").val(json.eobjResponse.tokenUrl);
                        $("#view-cred-service").modal('show');

                    } else {
                        $('.credServiceResponse').empty();
                        $('.credServiceResponse').append(json.responseMessage);
                        $("#showSpinnerAlert").modal('hide');
                        $('#spinner').hide();
                        $('#cred-service' + serviceId).modal('hide');
                        $("#failCredAlert").modal('show');
                        $("#failCredAlert").on("hidden.bs.modal", function() {
                            $(".credServiceResponse").html("");
                        });
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('.credServiceResponse').empty();
                    $('.credServiceResponse').append(errorThrown);
                    $("#showSpinnerAlert").modal('hide');
                    $('#spinner').hide();
                    $('#cred-service' + serviceId).modal('hide');
                    $("#failCredAlert").modal('show');
                    $("#failCredAlert").on("hidden.bs.modal", function() {
                        $(".credServiceResponse").html("");
                    });
                }
            });
        }
    </script>

    <!-- JavaScript -->
    <?php require_once('includes/footer.php'); ?>
</body>

</html>