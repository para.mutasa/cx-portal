<?php 
//session_start();
?>
<!DOCTYPE html>
<html lang="zxx" class="js">

<head>
    <base href="../../">
    <meta charset="utf-8">
    <meta name="author" content="Softnio">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="clean city.">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="././images/favicon.png">
    <!-- Page Title  -->
    <title>EcoCash Holdings - Leading PAN Africa Technology solutions group</title>
    <!-- StyleSheets  -->
    <link rel="stylesheet" href="././assets/css/dashlite.css">
    <link id="skin-default" rel="stylesheet" href="././assets/css/theme.css">
    <link rel="stylesheet" href="././assets/validate-password-requirements/css/jquery.passwordRequirements.css" />
    <style>
        #spinner{
            display:none; 
        }
        .invoice-desc .title {
    text-transform: uppercase;
    color: #0169a6;
}
.invoice-bills .table th {
    color: #0169a6;
    font-size: 12px;
    text-transform: uppercase;
    border-top: 0;
}
.text-primary {
    color: #0169a6 !important;
}

.btn-primary {
    color: #fff;
    background: linear-gradient( 89deg, #154abd 0.1%, #1a8ad3 51.5%, #48b1ea 100.2%);
    border-color: #888888;
}

.bg-light {
    /* background-color: #6576ff !important; */
    background: linear-gradient( 89deg, #6576ff 0.1%, #6576ff 51.5%, #48b1ea 100.2%);
}

.code_hide { display: none; }

.error{
color: #e85347;
font-size: 11px;
font-style: italic;
}

#body_background{
background: url("././images/stock/eco_image.jpg") no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
        }

        .passwordInput{          
       margin-top: 5%;           
      /* text-align :center; */   
         }     
         .displayBadge{ 
         margin-top: 5%; 
         display: none;            
     text-align :center;      
         }
    </style>

    

</head>


