<div class="nk-sidebar nk-sidebar-fixed is-dark " data-content="sidebarMenu">
                <div class="nk-sidebar-element nk-sidebar-head">
                    <div class="nk-menu-trigger">
                        <a href="#" class="nk-nav-toggle nk-quick-nav-icon d-xl-none" data-target="sidebarMenu"><em class="icon ni ni-arrow-left"></em></a>
                        <a href="#" class="nk-nav-compact nk-quick-nav-icon d-none d-xl-inline-flex" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
                    </div>
                    <div class="nk-sidebar-brand">
                        <a href="portal/admin/surveys" class="logo-link nk-sidebar-logo">
                        <h4 style="color:#fff;"> <span class="nk-menu-text">CX PORTAL</span></h4>

                        </a>
                    </div>
                </div><!-- .nk-sidebar-element -->
                <div class="nk-sidebar-element nk-sidebar-body">
                    <div class="nk-sidebar-content">
                        <div class="nk-sidebar-menu" data-simplebar>
                            <ul class="nk-menu">
                            <?php if($_SESSION['access_type'] == "SUPERADMIN"){ ?>

                          <!-- CX ADMIN -->
                          <!-- .nk-menu-item -->
                          <li class="nk-menu-item has-sub">
                                    <a href="#" class="nk-menu-link nk-menu-toggle">
                                        <span class="nk-menu-icon"><em class="icon ni ni-calendar-booking-fill"></em></span>
                                        <span class="nk-menu-text">Surveys</span>
                                    </a>
                                    <ul class="nk-menu-sub">
                                        <li class="nk-menu-item">
                                            <a href="portal/admin/surveys" class="nk-menu-link"><span class="nk-menu-text">All Surveys</span></a>
                                        </li>
                                    </ul>
                                </li><!-- .nk-menu-item -->
   

                                <li class="nk-menu-item has-sub">
                                    <a href="#" class="nk-menu-link nk-menu-toggle">
                                    <span class="nk-menu-icon"><em class="icon ni ni-card-view"></em></span>
                                        <span class="nk-menu-text">Services</span>
                                    </a>
                                    <ul class="nk-menu-sub">
                                 
                                        <li class="nk-menu-item">
                                            <a href="portal/admin/service-list" class="nk-menu-link"><span class="nk-menu-text">Service List</span></a>
                                        </li>
                                    
                                    </ul>
                                </li><!-- .nk-menu-item -->
                              
                       
                                <li class="nk-menu-item has-sub">
                                    <a href="#" class="nk-menu-link nk-menu-toggle">
                                    <span class="nk-menu-icon"><em class="icon ni ni-users"></em></span>
                                        <span class="nk-menu-text">User Manage</span>
                                    </a>
                                    <ul class="nk-menu-sub">
                                    <li class="nk-menu-item">
                                            <a href="portal/admin/user-list" class="nk-menu-link"><span class="nk-menu-text">All Users</span></a>
                                        </li>
                                   
                                    </ul><!-- .nk-menu-sub -->
                                </li><!-- .nk-menu-item -->


                                <li class="nk-menu-item has-sub">
                                    <a href="#" class="nk-menu-link nk-menu-toggle">
                                        <span class="nk-menu-icon"><em class="icon ni ni-coins"></em></span>
                                        <span class="nk-menu-text">Configs</span>
                                    </a>
                                    <ul class="nk-menu-sub">
                                 
                                        <!-- <li class="nk-menu-item">
                                            <a href="portal/admin/configs" class="nk-menu-link"><span class="nk-menu-text">General</span></a>
                                        </li> -->

                                        <li class="nk-menu-item">
                                            <a href="portal/admin/rate-limit" class="nk-menu-link"><span class="nk-menu-text">Rate Limiting</span></a>
                                        </li>
                                    
                                    </ul>

                                
                                </li><!-- .nk-menu-item -->

                                <li class="nk-menu-item has-sub">
                                    <a href="#" class="nk-menu-link nk-menu-toggle">
                                    <span class="nk-menu-icon"><em class="icon ni ni-reports"></em></span>
                                        <span class="nk-menu-text">Reports</span>
                                    </a>
                                    <ul class="nk-menu-sub">
                                 
                                        <li class="nk-menu-item">
                                            <a href="portal/admin/user-responses" class="nk-menu-link"><span class="nk-menu-text">Responses</span></a>
                                        </li>
                                    
                                    </ul>

                                
                                </li><!-- .nk-menu-item -->

                                <?php } else { ?>

                                    <!-- IS ADMIN -->
                                   <!-- .nk-menu-item -->
                                  <li class="nk-menu-item has-sub">
                                    <a href="#" class="nk-menu-link nk-menu-toggle">
                                        <span class="nk-menu-icon"><em class="icon ni ni-calendar-booking-fill"></em></span>
                                        <span class="nk-menu-text">Surveys</span>
                                    </a>
                                    <ul class="nk-menu-sub">
                                        <li class="nk-menu-item">
                                            <a href="portal/admin/surveys" class="nk-menu-link"><span class="nk-menu-text">All Surveys</span></a>
                                        </li>
                                    </ul>
                                </li><!-- .nk-menu-item -->
   

                                <!-- <li class="nk-menu-item has-sub">
                                    <a href="#" class="nk-menu-link nk-menu-toggle">
                                        <span class="nk-menu-icon"><em class="icon ni ni-coins"></em></span>
                                        <span class="nk-menu-text">Services</span>
                                    </a>
                                    <ul class="nk-menu-sub">
                                 
                                        <li class="nk-menu-item">
                                            <a href="portal/admin/service-list" class="nk-menu-link"><span class="nk-menu-text">Service List</span></a>
                                        </li>
                                    
                                    </ul>
                                </li> -->
                                <!-- .nk-menu-item -->
                              
                       
                                <!-- <li class="nk-menu-item has-sub">
                                    <a href="#" class="nk-menu-link nk-menu-toggle">
                                        <span class="nk-menu-icon"><em class="icon ni ni-coin"></em></span>
                                        <span class="nk-menu-text">User Manage</span>
                                    </a>
                                    <ul class="nk-menu-sub">
                                    <li class="nk-menu-item">
                                            <a href="portal/admin/user-list" class="nk-menu-link"><span class="nk-menu-text">All Users</span></a>
                                        </li>
                                   
                                    </ul>
                                </li> -->
                                <!-- .nk-menu-item -->


                                <li class="nk-menu-item has-sub">
                                    <a href="#" class="nk-menu-link nk-menu-toggle">
                                        <span class="nk-menu-icon"><em class="icon ni ni-coins"></em></span>
                                        <span class="nk-menu-text">Configs</span>
                                    </a>
                                    <ul class="nk-menu-sub">
                                 
                                        <!-- <li class="nk-menu-item">
                                            <a href="portal/admin/configs" class="nk-menu-link"><span class="nk-menu-text">General</span></a>
                                        </li> -->

                                        <li class="nk-menu-item">
                                            <a href="portal/admin/rate-limit" class="nk-menu-link"><span class="nk-menu-text">Rate Limiting</span></a>
                                        </li>
                                    
                                    </ul>

                                
                                </li><!-- .nk-menu-item -->

                                <!-- <li class="nk-menu-item has-sub">
                                    <a href="#" class="nk-menu-link nk-menu-toggle">
                                        <span class="nk-menu-icon"><em class="icon ni ni-coins"></em></span>
                                        <span class="nk-menu-text">Reports</span>
                                    </a>
                                    <ul class="nk-menu-sub">
                                 
                                        <li class="nk-menu-item">
                                            <a href="portal/admin/user-responses" class="nk-menu-link"><span class="nk-menu-text">Responses</span></a>
                                        </li>
                                    
                                    </ul> -->

                                
                                </li><!-- .nk-menu-item -->
                                <?php } ?>
                          
                            </ul><!-- .nk-menu -->
                        </div><!-- .nk-sidebar-menu -->
                    </div><!-- .nk-sidebar-content -->
                </div><!-- .nk-sidebar-element -->
            </div>