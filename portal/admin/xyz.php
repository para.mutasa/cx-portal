<?php 

include_once('../../utils/EcoCashHoldingsCxUtility.php');
require_once('includes/header.php');
$users= json_decode(getAllUsers(), true);

?>

<body class="nk-body bg-lighter npc-general has-sidebar ">
    <div class="nk-app-root">
        <!-- main @s -->
 
                <!-- main header @s -->
                <div class="nk-header nk-header-fixed is-light">
                    <div class="container-fluid">
                        <div class="nk-header-wrap">
                            <div class="nk-menu-trigger d-xl-none ml-n1">
                                <a href="#" class="nk-nav-toggle nk-quick-nav-icon" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
                            </div>
                            <div class="nk-header-brand d-xl-none">
                                <a href="portal/admin/index" class="logo-link">
                                    <img class="logo-light logo-img" src="./images/logo.png" srcset="./images/logo2x.png 2x" alt="logo">
                                    <img class="logo-dark logo-img" src="./images/logo-dark.png" srcset="./images/logo-dark2x.png 2x" alt="logo-dark">
                                </a>
                            </div><!-- .nk-header-brand -->
                            <div class="nk-header-news d-none d-xl-block">
                                <div class="nk-news-list">
                                    <a class="nk-news-item" href="#">
                                
                                    </a>
                                </div>
                            </div><!-- .nk-header-news -->
                            <div class="nk-header-tools">
                                <ul class="nk-quick-nav">
                                    <li class="dropdown user-dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <div class="user-toggle">
                                                <div class="user-avatar sm">
                                                    <em class="icon ni ni-user-alt"></em>
                                                </div>
                                                <div class="user-info d-none d-md-block">
                                                    <div class="user-status">Administrator</div>
                                                    <div class="user-name dropdown-indicator"><?php echo $admin_firstname;?></div>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right dropdown-menu-s1">
                                            <div class="dropdown-inner user-card-wrap bg-lighter d-none d-md-block">
                                                <div class="user-card">
                                                    <div class="user-avatar">
                                                        <span>AB</span>
                                                    </div>
                                                    <div class="user-info">
                                                    <span class="lead-text"><?php echo  $admin_firstname;?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                    <!-- <li><a href="#"><em class="icon ni ni-user-alt"></em><span>View Profile</span></a></li>
                                                    <li><a href="html/hotel/settings.html"><em class="icon ni ni-setting-alt"></em><span>Account Setting</span></a></li>
                                                    <li><a href="html/hotel/settings-activity-log.html"><em class="icon ni ni-activity-alt"></em><span>Login Activity</span></a></li> -->
                                                    <li><a class="dark-switch" href="#"><em class="icon ni ni-moon"></em><span>Dark Mode</span></a></li>
                                                </ul>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                <li><a href="portal/admin/logout"><em class="icon ni ni-signout"></em><span>Sign out</span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li><!-- .dropdown -->
                      
                                </ul><!-- .nk-quick-nav -->
                            </div><!-- .nk-header-tools -->
                        </div><!-- .nk-header-wrap -->
                    </div><!-- .container-fliud -->
                </div>
                <!-- main header @e -->
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="container-fluid">
                        <div class="nk-content-inner">
                            <div class="nk-content-body">
                                <div class="nk-block-head nk-block-head-sm">
                                    <div class="nk-block-between">
                                        <div class="nk-block-head-content">
                                            <h3 class="nk-block-title page-title">Users List</h3>
                                            <div class="nk-block-des text-soft">
                                                <!-- <p>You have total 1 Currency.</p> -->
                                            </div>
                                        </div><!-- .nk-block-head-content -->
                                        <div class="nk-block-head-content">
                                            <div class="toggle-wrap nk-block-tools-toggle">
                                                <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="pageMenu"><em class="icon ni ni-menu-alt-r"></em></a>
                                                <div class="toggle-expand-content" data-content="pageMenu">
                                                    <ul class="nk-block-tools g-3">
                                                    <li class="nk-block-tools-opt"><a href="#"  data-toggle="modal" data-target="#add-user" class="btn btn-primary"><em class="icon ni ni-reports"></em><span>Add User</span></a></li>
                                                    
                                                    </ul>
                                                </div>
                                            </div><!-- .toggle-wrap -->
                                        </div><!-- .nk-block-head-content -->
                                    </div><!-- .nk-block-between -->
                                </div><!-- .nk-block-head -->

                                <div class="nk-block nk-block-lg">
                                    
                                        <div class="card card-preview">
                                            <div class="card-inner">
                                            <table class="datatable-init nowrap nk-tb-list nk-tb-ulist" data-order='[[0, "desc"]]' data-auto-responsive="false">
                                                    <thead>
                                                        <tr class="nk-tb-item nk-tb-head">
                                                            <th class="nk-tb-col"><span class="sub-text">Name</span></th>
                                                            <th class="nk-tb-col tb-col-md"><span class="sub-text">Phone Number</span></th>
                                                            <th class="nk-tb-col tb-col-md"><span class="sub-text">Email</span></th>
                                                            <th class="nk-tb-col tb-col-md"><span class="sub-text">User Type</span></th>
                                                            <th class="nk-tb-col tb-col-md"><span class="sub-text">Account Status</span></th>
                                                            <th class="nk-tb-col nk-tb-col-tools text-right">
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php if ($users['responseStatus'] == "SUCCESS") : ?>
                                                    <?php
                                                    foreach ($users['eobjResponse'] as $user) :
                                                    ?>
                                                        <tr>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $user['firstName']." ".$user['lastName']  ?></span></td>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $user['mobileNumber'] ?></span></td>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $user['email'] ?></span></td>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $user['accessType'] ?></span></td>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $user['status'] ?></span></td>
                                                        <td class="nk-tb-col nk-tb-col-tools">
                                                            <ul class="nk-tb-actions gx-1">

                                                                <li>
                                                                    <div class="drodown">
                                                                        <a href="#" class="dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
                                                                        <div class="dropdown-menu dropdown-menu-right">
                                                                            <ul class="link-list-opt no-bdr">
                                                                            <li><a href="#" data-toggle="modal" data-target="#edit-user<?= $user['userId'] ?>"><em class="icon ni ni-edit"></em><span>Edit User</span></a></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </td>
                                                    <!-- Edit User-->
                                                    <div class="modal fade" tabindex="-1" role="dialog" id="edit-user<?= $user['userId'] ?>">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                                                            <div class="modal-body modal-body-md">
                                                            <div class="gy-4">
                                                            <div class="example-alert">
                                                                <div class="alert alert-pro alert-primary">
                                                                    <div class="alert-text">
                                                                        <h5 class="modal-title">Edit User</h5>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            </div>
                                                                <form id ="edit-user<?= $user['userId'] ?>" class="mt-2">
                                                                    <div class="row g-gs">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Username</label>
                                                                                <input type="text" class="form-control"  name ="userName" value="<?= $user['userName'] ?>">
                                                                            </div>
                                                                        </div>
                                                                    
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">First Name</label>
                                                                                <input type="text" class="form-control"  name ="firstName" value="<?= $user['firstName'] ?>">
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Last Name</label>
                                                                                <input type="text" class="form-control"  name ="lastName" value="<?= $user['lastName'] ?>">
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-6">
                                                                        <div class="form-group">
                                                                                <label class="form-label">Access Type</label>
                                                                                <div class="form-control-wrap">
                                                                                    <select name="accessType" class="form-select" data-placeholder="Select accessType" >
                                                                                    <option value="ADMIN">IS ADMIN</option>
                                                                                    <option value="SUPERADMIN">CX ADMIN</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label">Status</label>
                                                                                <div class="form-control-wrap">
                                                                                    <select name="status" class="form-select" data-placeholder="Select Status" >
                                                                                    <option value="ACTIVE">ACTIVE</option>
                                                                                    <option value="CLOSED">CLOSED</option>
                                                                                    <option value="DELETED">DELETED</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Email</label>
                                                                                <input type="text" class="form-control"  name ="email" value="<?= $user['email'] ?>">
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Mobile Number</label>
                                                                                <input type="text" class="form-control"  name ="mobileNumber" value="<?= $user['mobileNumber'] ?>">
                                                                            </div>
                                                                        </div>

                                                                        <input type="hidden" class="form-control" name="lastModifiedBy" value="<?php echo $admin_email; ?>" >
                                                                        <input type="hidden" class="form-control"  name="userId" value="<?= $user['userId'] ?>">
                                                                        
                                                                        <div class="col-12">
                                                                            <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                                                                <li>
                                                                                <input type="hidden" name="edit_user" value="true">
                                                                                <button type="button" class="btn btn-primary" name ="edit_user"  onClick="editUser('<?= $user["userId"] ?>')" >Submit</button>
                                                                                </li>
                                                                              
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div><!-- .modal-body -->
                                                            <div class="modal-footer bg-light">
                                                        <span class="sub-text"></span>
                                                    </div>
                                                        </div><!-- .modal-content -->
                                                    </div><!-- .modal-dialog -->
                                                    </div><!-- .modal -->

                                                    <?php endforeach; ?>

                                                    <?php else : ?>

                                                    <?= $users['responseMessage'] ?>

                                                    <?php endif; ?>

                                                     
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div><!-- .card-preview -->
                                    </div> <!-- nk-block -->

                               
                            </div>
                        </div>
                    </div>
                </div>
                <!-- content @e -->
                <!-- footer @s -->
                <div class="nk-footer">
                    <div class="container-fluid">
                        <div class="nk-footer-wrap">
                        <div class="nk-footer-copyright"> &copy; Copyright Ecocash Holdings Zimbabwe 2022 <a href="https://www.ecocashholdings.co.zw/" target="_blank"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- footer @e -->
            </div>
            <!-- wrap @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->
      <!-- Success Modal Alert -->
      <div class="modal fade" tabindex="-1" id="successAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                        <div class="caption-text">You’ve successfully added user</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!--Fail  Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                        <div class='addUserResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>


     <!-- Success Modal Alert -->
     <div class="modal fade" tabindex="-1" id="successUserAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                        <div class="caption-text">You’ve successfully updated user</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!--Fail  Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failUserAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                        <div class='updateSurveyResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
      <!-- Success Modal Alert -->
      <div class="modal fade" tabindex="-1" id="successAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                        <div class="caption-text">You’ve successfully added survey</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!--Fail  Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                        <div class='addSurveyResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Success Edit Modal Alert -->
    <div class="modal fade" tabindex="-1" id="successEditAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                        <div class="caption-text">You’ve successfully created question</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!--Fail  Edit Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failEditAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                        <div class='createQuestionResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- Success Edit Modal Alert -->
    <div class="modal fade" tabindex="-1" id="successEditAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                        <div class="caption-text">You’ve successfully updated currency</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!--Fail  Edit Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failEditAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                        <div class='editCurrencyResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Add User-->
    <div class="modal fade" tabindex="-1" role="dialog" id="add-user">
        <div class="modal-dialog modal-mb" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                <div class="modal-body modal-body-md">
                <div class="gy-4">
                <div class="example-alert">
                    <div class="alert alert-pro alert-primary">
                        <div class="alert-text">
                            <h5 class="modal-title">Add User</h5>
                        </div>
                    </div>
                </div>
                </div>
                    <!-- <div class="d-flex justify-content-center">  <div class="spinner-border" id="spinner" style="width: 4rem; height: 4rem;" role="status">    <span class="sr-only">Loading...</span>  </div></div> -->
                    <form id = "adduser" action="#" class="mt-2">
                        <div class="row g-gs">
                        <div class="col-md-6">
                            <div class="form-group">
                                    <label class="form-label" for="payment-name-add">Username</label>
                                    <input type="text" class="form-control"  name ="userName" placeholder="">
                                </div>
                            </div>
                            <div class="col-md-6">
                            <div class="form-group">
                                    <label class="form-label" for="payment-name-add">First Name</label>
                                    <input type="text" class="form-control"  name ="firstName" placeholder="">
                                </div>
                            </div>
                            <div class="col-md-6">
                            <div class="form-group">
                                    <label class="form-label" for="payment-name-add">Last Name</label>
                                    <input type="text" class="form-control"  name ="lastName" placeholder="">
                                </div>
                            </div>
                            <div class="col-md-6">
                            <div class="form-group">
                                    <label class="form-label" for="payment-name-add">Email</label>
                                    <input type="email" class="form-control"  name ="email" placeholder="">
                                </div>
                            </div>
                            <div class="col-md-6">
                            <div class="form-group">
                                    <label class="form-label" for="payment-name-add">Mobile Number</label>
                                    <input type="text" class="form-control"  name ="mobileNumber" placeholder="">
                                </div>
                            </div>
                            <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">Access Type</label>
                            <div class="form-control-wrap">
                                <select name="accessType" class="form-select" data-placeholder="Select Access Type" >
                                <option value="ADMIN">IS ADMIN</option>
                                <option value="SUPERADMIN">CX ADMIN</option>
                                </select>
                            </div>
                        </div>
                        </div>
                            <!-- <div class="col-md-6">
                            <div class="form-group">
                                    <label class="form-label" for="payment-name-add">Password</label>
                                    <input type="password" class="form-control passwordInput" name="password" id="PassEntry" required>
                            <span id="StrengthDisp" class="badge displayBadge">Weak</span>
                                </div>
                            </div> -->
                          
                        
                            <div class="col-10">
                                <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                    <li>
                                        <input type="hidden" class="form-control" name="add_user">
                                        <button type="button" class="btn btn-primary" name ="add_user" onClick="addUser()">Submit</button>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </form>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-light">
                    <span class="sub-text"></span>
                </div>
            </div><!-- .modal-content -->
        </div><!-- .modal-dialog -->
    </div><!-- .modal -->
    <script src="./assets/validate-password-requirements/js/jquery.passwordRequirements.min.js"></script>
    
    <script>

let timeout;

    // traversing the DOM and getting the input and span using their IDs
let password = document.getElementById('PassEntry');
    let strengthBadge = document.getElementById('StrengthDisp');

    // The strong and weak password Regex pattern checker
let strongPassword = new RegExp('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{8,})');
    let mediumPassword = new RegExp('((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{6,}))|((?=.*[a-z])(?=.*[A-Z])(?=.*[^A-Za-z0-9])(?=.{8,}))');
    
    function StrengthChecker(PasswordParameter){
        // We then change the badge's color and text based on the password strength
if(strongPassword.test(PasswordParameter)) {
            strengthBadge.style.backgroundColor = 'green';
            strengthBadge.textContent = 'Strong';
        } else if(mediumPassword.test(PasswordParameter)){
            strengthBadge.style.backgroundColor = 'blue';
            strengthBadge.textContent = 'Medium';
        } else{
            strengthBadge.style.backgroundColor = 'red';
            strengthBadge.textContent = 'Weak';
        }
    }

password.addEventListener("input", () => {
    strengthBadge.style.display = 'block';
    clearTimeout(timeout);
    timeout = setTimeout(() => StrengthChecker(password.value), 500);
    if(password.value.length !== 0) {
        strengthBadge.style.display != 'block';
    } else {
        strengthBadge.style.display = 'none';
    }
});


</script>
       <!-- BEGIN: AJAX CALLS-->
   <script>
    //Add User
    function addUser() {
        var PasswordParameter = document.getElementById('PassEntry').value;

var vMsg = 'The minimum password length is 8 characters and must contain at least 1 lowercase letter, 1 capital letter, 1 number, and 1 special character.';

if(!strongPassword.test(PasswordParameter)) {

alert(vMsg); return;

}
        $.ajax({
            type: "POST",
            url: "portal/survey/controller/process.php",
            data: $('form#adduser').serialize(),
            cache: false,
            success: function(response) {
                var json = $.parseJSON(response);
                if (json.responseStatus == "SUCCESS") {
                    $('#add-user').modal('hide');
                    $("#successAlert").modal('show');
                    setTimeout(function() {
                        window.location = "portal/admin/xyz";
                    }, 2000);
                } else {
                    $('.addUserResponse').empty();
                    $('.addUserResponse').html(json.responseMessage);
                    $('#add-user').modal('hide');
                    $("#failAlert").modal('show');
                    $("#failAlert").on("hidden.bs.modal", function() {
                        $(".addUserResponse").html("");
                    });
                    
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.addUserResponse').empty();
                $('.addUserResponse').append(errorThrown);
                $('#add-user').modal('hide');
                $("#failAlert").modal('show');
                $("#failAlert").on("hidden.bs.modal", function() {
                    $(".addUserResponse").html("");
                });
            }
        });
    }
    </script>
    <script>
    
   //edit User
   function editUser(userId) {
        $.ajax({
            type: "POST",
            url: "portal/survey/controller/process.php",
            data: $('form#edit-user'+userId).serialize(),
            cache: false,
            success: function(response) {
                var json = $.parseJSON(response);
                if (json.responseStatus == "SUCCESS") {
                    $('#edit-user'+userId).modal('hide');
                    $("#successUserAlert").modal('show');
                    setTimeout(function() {
                        window.location = "portal/admin/xyz";
                    }, 2000);
                  
                } else {
                    $('.updateUserResponse').empty();
                    $('.updateUserResponse').append(json.responseMessage);
                    $('#edit-user'+userId).modal('hide');
                    $("#failUserAlert").modal('show');
                    $("#failUserAlert").on("hidden.bs.modal", function() {
                        $(".updateUserResponse").html("");
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.updateUserResponse').empty();
                $('.updateUserResponse').append(errorThrown);
                $('#edit-user'+userId).modal('hide');
                $("#failUserAlert").modal('show');
                $("#failUserAlert").on("hidden.bs.modal", function() {
                    $(".updateUserResponse").html("");
                });
            }
        });
    }
    </script>
   <!-- JavaScript -->
   <?php require_once('includes/footer.php');?>
</body>

</html>