<?php
include_once('../../utils/EcoCashHoldingsCxUtility.php');
require_once('includes/header.php');
$services = json_decode(getAllServices(), true);

$surveyId = $_GET['surveyId'];

$questions = json_decode(getAllSurveyQuestionsById($surveyId), true)['eobjResponse']['questions'];

// var_dump($questions);
// exit;

// $usd_amount = json_decode(getAllSurveyQuestionsById($surveyId),true)['eobjResponse'];
// var_dump($usd_amount);
// exit;
?>


<body class="nk-body bg-lighter npc-general has-sidebar " id="body_background" onload="disableReason()">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">

            <!-- wrap @s -->
            <div class="nk-wrap ">
                <!-- main header @s -->
                <!-- <div class="nk-header nk-header-fixed is-light">
                    <div class="container-fluid">
                        <div class="nk-header-wrap">
                            <div class="nk-menu-trigger d-xl-none ml-n1">
                                <a href="#" class="nk-nav-toggle nk-quick-nav-icon" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
                            </div>
                            <div class="nk-header-brand d-xl-none">
                                <a href="html/index.html" class="logo-link">
                                    <img class="logo-light logo-img" src="./images/vaya_logo_white.png" srcset="./images/vaya_logo_white.png 2x" alt="logo">
                                    <img class="logo-dark logo-img" src="./images/vaya_logo_white.png" srcset="./images/vaya_logo_white.pngg 2x" alt="logo-dark">
                                </a>
                            </div>
                            
                          
                         
                        </div>
                    </div>
                </div> -->
                <!-- main header @e -->
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="container-fluid">
                        <div class="nk-content-inner">
                            <div class="nk-content-body">
                                <div class="components-preview wide-md mx-auto">
                                    <div class="nk-block-head nk-block-head-lg wide-md">
                                        <div class="card text-white bg-secondary">
                                            <div class="card-inner">
                                                <h2 class="card-title">Welcome to EcoCash Holdings</h2>
                                                <p class="card-text">The business would like to get feedback on ways of further enhancing our processes in a bid to satisfy our customers. May you kindly respond to the questions as accurately as possible. Your answers will be kept anonymous.</p>
                                            </div>
                                        </div>

                                    </div><!-- .nk-block-head -->



                                    <div class="nk-block nk-block-lg npc-general pg-auth">

                                        <div class="card card-bordered">
                                            <div class="card-inner">
                                                <form action="#" class="form-validate is-alter" id="save_response">

                                                    <div class="row g-gs">
                                                        <!-- <div class="col-md-6" id ="phone_number">
                                                        <div class="form-group">
                                                            <label class="form-label" for="payment-name-add">Mobile Number</label>
                                                            <input type="text" class="form-control"  name ="questionNumber" >
                                                        </div>
                                                    </div> -->


                                                        <div class="col-md-10">
                                                            <div class="form-group">
                                                                <label class="form-label">Select Service</label>
                                                                <div class="form-control-wrap">
                                                                    <select class="form-control" name="serviceName" required>
                                                                        <?php foreach ($services['eobjResponse']  as $service) : ?>
                                                                            <option value="<?= $service["serviceName"] ?>"><?= $service["serviceName"] . "" ?></option>
                                                                        <?php endforeach; ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-12">
                                                            <label class="form-label" for="fva-topics">(1)<?php echo json_decode(getAllSurveyQuestionsById($surveyId), true)['eobjResponse']['questions'][0]['question']; ?><li></li></label>
                                                            <div class="card card-preview">
                                                                <div class="card-inner">
                                                                    <div class="row gy-4">

                                                                        <div class="col-md-3-sm col-sm-1">
                                                                            <div class="preview-block">

                                                                                <span class="preview-title overline-title">1</span>
                                                                                <div class="custom-control custom-radio">
                                                                                    <input type="radio" id="customRadio42" name="response_one" value="1" class="custom-control-input">
                                                                                    <label class="custom-control-label" for="customRadio42"></label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3-sm col-sm-1">
                                                                            <div class="preview-block">
                                                                                <span class="preview-title overline-title">2</span>
                                                                                <div class="custom-control custom-radio">
                                                                                    <input type="radio" id="customRadio2" name="response_one" value="2" class="custom-control-input">
                                                                                    <label class="custom-control-label" for="customRadio2"></label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3-sm col-sm-1">
                                                                            <div class="preview-block">
                                                                                <span class="preview-title overline-title">3</span>
                                                                                <div class="custom-control custom-radio">
                                                                                    <input type="radio" id="customRadio3" name="response_one" value="3" class="custom-control-input">
                                                                                    <label class="custom-control-label" for="customRadio3"></label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3-sm col-sm-1">
                                                                            <div class="preview-block">
                                                                                <span class="preview-title overline-title">4</span>
                                                                                <div class="custom-control custom-radio">
                                                                                    <input type="radio" id="customRadio4" name="response_one" value="4" class="custom-control-input">
                                                                                    <label class="custom-control-label" for="customRadio4"></label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3-sm col-sm-1 ">
                                                                            <div class="preview-block">
                                                                                <span class="preview-title overline-title">5</span>
                                                                                <div class="custom-control custom-radio">
                                                                                    <input type="radio" id="customRadio5" name="response_one" value="5" class="custom-control-input">
                                                                                    <label class="custom-control-label" for="customRadio5"></label>
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-12">
                                                            <label class="form-label" for="fva-topics">(2)<?php echo json_decode(getAllSurveyQuestionsById($surveyId), true)['eobjResponse']['questions'][1]['question']; ?> </label>
                                                            <div class="card card-preview">
                                                                <div class="card-inner">
                                                                    <div class="row gy-4">
                                                                        <div class="col-md-3-sm col-sm-1">

                                                                            <div class="preview-block">
                                                                                <span class="preview-title overline-title">1</span>
                                                                                <div class="custom-control custom-radio">
                                                                                    <input type="radio" id="customRadio11" name="response_two" value="1" class="custom-control-input">
                                                                                    <label class="custom-control-label" for="customRadio11"><b></b></label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3-sm col-sm-1">
                                                                            <div class="preview-block">
                                                                                <span class="preview-title overline-title">2</span>
                                                                                <div class="custom-control custom-radio">
                                                                                    <input type="radio" id="customRadio12" name="response_two" value="2" class="custom-control-input">
                                                                                    <label class="custom-control-label" for="customRadio12"></label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3-sm col-sm-1">
                                                                            <div class="preview-block">
                                                                                <span class="preview-title overline-title">3</span>
                                                                                <div class="custom-control custom-radio">
                                                                                    <input type="radio" id="customRadio13" name="response_two" value="3" class="custom-control-input">
                                                                                    <label class="custom-control-label" for="customRadio13"></label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3-sm col-sm-1">
                                                                            <div class="preview-block">
                                                                                <span class="preview-title overline-title">4</span>
                                                                                <div class="custom-control custom-radio">
                                                                                    <input type="radio" id="customRadio14" name="response_two" value="4" class="custom-control-input">
                                                                                    <label class="custom-control-label" for="customRadio14"></label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3-sm col-sm-1">
                                                                            <div class="preview-block">
                                                                                <span class="preview-title overline-title">5</span>
                                                                                <div class="custom-control custom-radio">
                                                                                    <input type="radio" id="customRadio15" name="response_two" value="5" class="custom-control-input">
                                                                                    <label class="custom-control-label" for="customRadio15"></label>
                                                                                </div>
                                                                            </div>
                                                                        </div>



                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div><!-- .card-preview -->


                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="form-label" for="fva-topics">(3)<?php echo json_decode(getAllSurveyQuestionsById($surveyId), true)['eobjResponse']['questions'][2]['question']; ?></label>
                                                                <div class="form-control-wrap ">
                                                                    <select class="form-control form-select" name="response_three" id="user_value" onChange="getValue();" data-placeholder="Select an option" required>
                                                                        <option label="" value="">Select an option</option>
                                                                        <option value="Very dissatisfied">Very dissatisfied</option>
                                                                        <option value="Somewhat dissatisfied">Somewhat dissatisfied</option>
                                                                        <option value="Neither Satisfied nor Dissatisfied">Neither Satisfied nor Dissatisfied</option>
                                                                        <option value="Somewhat satisfied">Somewhat satisfied</option>
                                                                        <option value="Very satisfied">Very satisfied</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12" id="hide_all">
                                                            <div class="form-group">
                                                                <label class="form-label" for="fva-message">Kindly Specify the reason</label>
                                                                <div class="form-control-wrap">
                                                                    <textarea class="form-control form-control-sm" id="message" name="reason" placeholder="Write your message" required></textarea>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="form-label" for="fva-message">(4)<?php echo json_decode(getAllSurveyQuestionsById($surveyId), true)['eobjResponse']['questions'][3]['question']; ?></label>
                                                                <div class="form-control-wrap">
                                                                    <textarea class="form-control form-control-sm" id="fva-message" name="response_four" placeholder="Write your message" required></textarea>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!-- <button type="button" class="btn btn-primary" name ="edit_survey"  onClick="editSurvey('<?= $survey["surveyId"] ?>')" >Edit</button>  -->
                                                        <button type="button" data-dismiss="modal" data-toggle="modal" data-target="#edit-questions" class="btn btn-primary">Modify Questions</button>

                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div><!-- .nk-block -->

                                </div><!-- .components-preview -->
                            </div>


                            <!-- update Question-->
                            <div class="modal fade" tabindex="-1" role="dialog" id="edit-questions">
                                <div class="modal-dialog modal-md" role="document">
                                    <div class="modal-content">
                                        <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                                        <div class="modal-body modal-body-lg">
                                            <div style="color: #1C7ACD; text-align: center;" class='editQuestionsResponse'></div>
                                            <h5 class="modal-title">Edit Question</h5>
                                            <form id="edit_question" class="mt-2">
                                                <div class="row g-gs">
                                                    <div class="col-md-10">
                                                        <div class="form-group">
                                                            <label class="form-label" for="payment-name-add">Question <?= $questions['0']['questionNumber'] ?></label>
                                                            <input type="text" class="form-control" name="question1" value="<?= $questions['0']['question'] ?> ">
                                                            <input type="hidden" class="form-control" name="questionId1" value="<?= $questions['0']['questionId'] ?>">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-10">
                                                        <div class="form-group">
                                                            <label class="form-label" for="payment-name-add">Question <?= $questions['1']['questionNumber'] ?></label>
                                                            <input type="text" class="form-control" name="question2" value="<?= $questions['1']['question'] ?>">
                                                            <input type="hidden" class="form-control" name="questionId2" value="<?= $questions['1']['questionId'] ?>">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-10">
                                                        <div class="form-group">
                                                            <label class="form-label" for="payment-name-add">Question <?= $questions['2']['questionNumber'] ?></label>
                                                            <input type="text" class="form-control" name="question3" value="<?= $questions['2']['question'] ?>">
                                                            <input type="hidden" class="form-control" name="questionId3" value="<?= $questions['2']['questionId'] ?>">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-10">
                                                        <div class="form-group">
                                                            <label class="form-label" for="payment-name-add">Question <?= $questions['3']['questionNumber'] ?></label>
                                                            <input type="text" class="form-control" name="question4" value="<?= $questions['3']['question'] ?>">
                                                            <input type="hidden" class="form-control" name="questionId4" value="<?= $questions['3']['questionId'] ?>">
                                                        </div>
                                                    </div>


                                                    <input type="hidden" class="form-control" name="lastModifiedBy" value="<?php echo  $_SESSION["admin_Email"]; ?>">
                                                    <input type="hidden" class="form-control" name="surveyId" value="<?= $questions['0']['surveyId'] ?>">
                                                    <div class="col-12">
                                                        <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                                            <li>
                                                                <input type="hidden" name="edit_question" value="true">
                                                                <button type="button" class="btn btn-primary" name="edit_question" onClick="editQuestions()">Edit Question</button>
                                                                <!-- <button type="submit" class="btn btn-primary" name ="edit_question"  >Edit Question</button> -->
                                                            </li>

                                                        </ul>
                                                    </div>
                                                </div>
                                            </form>
                                        </div><!-- .modal-body -->
                                        <div class="modal-footer bg-light">
                                            <span class="sub-text"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- content @e -->
                            <!-- footer @s -->
                            <div class="nk-footer">
                                <div class="container-fluid">
                                    <div class="nk-footer-wrap">
                                        <div class="nk-footer-copyright"> &copy; Copyright Ecocash Holdings Zimbabwe 2022 <a href="https://www.ecocashholdings.co.zw/" target="_blank"></a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- footer @e -->
                        </div>
                        <!-- wrap @e -->
                    </div>
                    <!-- main @e -->
                </div>
                <!-- app-root @e -->
                <div class="modal fade" tabindex="-1" id="successQuestionAlert">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                            <div class="modal-body modal-body-lg text-center">
                                <div class="nk-modal">
                                    <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                                    <h4 class="nk-modal-title">Success!</h4>
                                    <div class="nk-modal-text">
                                        <div class="caption-text">You’ve successfully updated Question </div>
                                    </div>
                                    <div class="nk-modal-action">
                                        <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                                    </div>
                                </div>
                            </div><!-- .modal-body -->
                            <div class="modal-footer bg-lighter">
                                <div class="text-center w-100">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" tabindex="-1" id="failQuestionAlert">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-body modal-body-lg text-center">
                                <div class="nk-modal">
                                    <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                                    <h4 class="nk-modal-title">Unable to Process!</h4>
                                    <div class="nk-modal-text">
                                        <div class='updateQuestionResponse'></div>
                                    </div>
                                    <div class="nk-modal-action mt-5">
                                        <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                                    </div>
                                </div>
                            </div><!-- .modal-body -->
                            <div class="modal-footer bg-lighter">
                                <div class="text-center w-100">

                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- JavaScript -->
                    <script src="./assets/js/bundle.js"></script>
                    <script src="./assets/js/scripts.js"></script>
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

</body>

<script>
    function disableReason() {
        document.getElementById('hide_all').style.display = 'none';
    }
</script>


<script>
    function getValue() {
        const user_value = document.getElementById("user_value").value;
        console.log(user_value);
        if (user_value == "Very dissatisfied") {
            document.getElementById('hide_all').style.display = 'inline-block';
        } else if (user_value == "Somewhat dissatisfied") {
            document.getElementById('hide_all').style.display = 'inline-block';
        } else if (user_value == "Neither Satisfied nor Dissatisfied") {
            document.getElementById('hide_all').style.display = 'inline-block';
        } else if (user_value == "") {
            document.getElementById('hide_all').style.display = 'none';
        } else {
            document.getElementById('hide_all').style.display = 'none';
        }
    }
</script>


<script>
    //Save User Response 
    function saveResponses() {
        $.ajax({
            type: "POST",
            url: "portal/survey/controller/process.php",
            data: $('form#save_response').serialize(),
            cache: false,
            success: function(response) {
                var json = $.parseJSON(response);
                if (json.responseStatus == "SUCCESS") {
                    setTimeout(function() {
                        window.location = "portal/survey/thank-you";
                    }, 2000);
                } else {
                    $('.saveUserResponse').empty();
                    $('.saveUserResponse').html(json.responseMessage);


                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.saveUserResponse').empty();
                $('.saveUserResponse').append(errorThrown);
            }
        });
    }
</script>

<script>
    //edit Question
    function editQuestions() {
        $.ajax({
            type: "POST",
            url: "portal/survey/controller/process.php",
            data: $('form#edit_question').serialize(),
            cache: false,
            success: function(response) {
                // console.log(response);
                var json = $.parseJSON(response);
                if (json.responseStatus == "SUCCESS") {
                    $('.editQuestionsResponse').empty(); //clear apend
                    $('.editQuestionsResponse').append("Questions updated Successfully");
                    setTimeout(function() {
                        location.reload();
                    }, 2000);

                } else {
                    $('.updateQuestionResponse').empty();
                    $('.editQuestionsResponse').empty(); //clear apend
                    $('.editQuestionsResponse').append("Please try again");
                    $("#failQuestionAlert").on("hidden.bs.modal", function() {
                        $(".updateQuestionResponse").html("");
                    });

                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.updateQuestionResponse').empty();
                $('.editQuestionsResponse').append("Please try again");
                $("#failQuestionAlert").on("hidden.bs.modal", function() {
                    $(".updateQuestionResponse").html("");
                });
            }
        });
    }
</script>

</html>