
<?php
  
  include_once('../utils/EcoCashHoldingsCxUtility.php');
  if(isset($_POST['reset']))
  {
      if(!empty($_POST['email']) && !empty($_POST['resetToken']) && !empty($_POST['newpassword']))
      {
          $email 		 = trim($_POST['email']);
          $resetToken 		 = trim($_POST['resetToken']);
          $newpassword 		 = trim($_POST['newpassword']);
          
          $resetPassword = json_decode(resetPassword($email,$resetToken,$newpassword),true);
        
          if($resetPassword['responseStatus'] == "SUCCESS"){
              header("location: admin-login");
              exit;
        
                     
          } else {
             
              $errorMsg = $resetPassword['responseMessage'];
          }
      }
  }
 

?>
<!DOCTYPE html>
<html lang="zxx" class="js">

<head>
    <base href="../">
    <meta charset="utf-8">
    <meta name="author" content="Softnio">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="clean city.">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="./images/favicon.png">
    <!-- Page Title  -->
    <title>EcoCash Holdings - Leading PAN Africa Technology solutions group</title>
    <!-- StyleSheets  -->
    <link rel="stylesheet" href="./assets/css/dashlite.css?ver=2.9.0">
    <link id="skin-default" rel="stylesheet" href="./assets/css/theme.css?ver=2.9.0">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js"></script>
    <script>
    function AES_CBC_encrypt() {
    const key = 'b!5Tb+EjdKyaYj6{M/atWSdkghjd8945'
    // var iv = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15 ]
    var iv = "1234567890000000";
    var message = document.getElementById("password").value;
    // utf8 character string —>WordArray object ,WordArray It's a save 32 An array of bit integers , Equivalent to binary
    let keyHex = CryptoJS.enc.Utf8.parse(key); //
    let ivHex = CryptoJS.enc.Utf8.parse(iv);
    let messageHex = CryptoJS.enc.Utf8.parse(message);
    let encrypted = CryptoJS.AES.encrypt(messageHex, keyHex, {
    iv: ivHex,
    mode: CryptoJS.mode.CBC,
    padding: CryptoJS.pad.Pkcs7
    });
    document.getElementById('password').value= encrypted.toString();
    return true;
    // return encrypted.toString();// base64 result
    //return encrypted.ciphertext.toString(); // Binary results
    }
    </script>
    <style>
        .btn-primary {
    color: #fff;
    background-color: #888888;
    border-color: #888888;
}
body {
    background: url("./images/stock/eco_image.jpg") no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
}

    </style>
</head>
<body class="nk-body bg-white npc-general pg-auth">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <!-- wrap @s -->
            <div class="nk-wrap nk-wrap-nosidebar">
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="nk-split nk-split-page nk-split-md">
                        <div class="nk-split-content nk-block-area nk-block-area-column nk-auth-container bg-white w-lg-45">
                            <div class="absolute-top-right d-lg-none p-3 p-sm-5">
                                <a href="#" class="toggle btn btn-white btn-icon btn-light" data-target="athPromo"><em class="icon ni ni-info"></em></a>
                            </div>
                            <div class="nk-block nk-block-middle nk-auth-body">
                                <div class="brand-logo pb-5">
                                    <a href="portal/index" class="logo-link">
                                    <img class="logo-light logo-img logo-img-md" src="./images/ecocash_logo.png" srcset="./images/ecocash_logo.png" alt="logo">
                                    <img class="logo-dark logo-img logo-img-md" src="./images/ecocash_logo.png" srcset="./images/ecocash_logo.png" alt="logo-dark">
                                 </a>
                                </div>
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <h5 class="nk-block-title">Reset password</h5>
                                    
                                    </div>
                                    <?php

                                    if(isset($errorMsg))

                                    {

                                    echo '<span style ="margin-left: -1%;"class="alert alert-danger">';

                                    echo $errorMsg;

                                    echo '</span>';

                                    unset($errorMsg);

                                    }

                                    ?>
                                </div><!-- .nk-block-head -->
                               
                                <form action="portal/admin-reset-password" method="post">
                                <div class="form-group">
                                        <div class="form-label-group">
                                            <label class="form-label" for="default-01">Email</label>
                                        </div>
                                        <div class="form-control-wrap">
                                            <!-- <input type="text" class="form-control form-control-lg"  id="default-01" placeholder="Enter your email address"> -->
                                            <input type="text" class="form-control"  name ="email" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-label-group">
                                            <label class="form-label" for="default-01">Token</label>
                                         
                                        </div>
                                        <div class="form-control-wrap">
                                            <input type="text" name="resetToken" class="form-control form-control-lg" id="default-01" placeholder="Enter your token" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-label-group">
                                            <label class="form-label" for="password">Password</label>
                                        </div>
                                        <div class="form-control-wrap">
                                            <a tabindex="-1" href="#" class="form-icon form-icon-right passcode-switch lg" data-target="password">
                                                <em class="passcode-icon icon-show icon ni ni-eye"></em>
                                                <em class="passcode-icon icon-hide icon ni ni-eye-off"></em>
                                            </a>
                                            <input type="password" class="form-control form-control-lg" name="newpassword" id="password"  pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" title="at least one number and one uppercase and lowercase letter, and at least 6 or more characters" placeholder="Enter your password" required>
                                        </div><br />
                                    </div><!-- .form-group -->
                                    <div class="form-group">
                                    <button type="submit" class="btn btn-primary"  onclick="return AES_CBC_encrypt()"  name="reset" >Reset</button>

                                    </div>
                                </form><!-- form -->
                                <div class="form-note-s2 pt-5">
                                    <a href="portal/admin-login"><strong>Return to login</strong></a>
                                </div>
                            </div><!-- .nk-block -->
                            <div class="nk-block nk-auth-footer">
                          
                                <div class="mt-3">
                                <p>&copy; Copyright Ecocash Holdings Zimbabwe 2022 <a href="https://www.ecocashholdings.co.zw/" target="_blank"></a></p>
                                </div>
                            </div><!-- .nk-block -->
                        </div><!-- .nk-split-content -->
                        <div class="nk-split-content nk-split-stretch bg-abstract"></div><!-- .nk-split-content -->
                    </div><!-- .nk-split -->
                </div>
                <!-- wrap @e -->
            </div>
            <!-- content @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->
    <!-- JavaScript -->
    <script src="./assets/js/bundle.js?ver=2.9.0"></script>
 <script src="./assets/js/scripts.js?ver=2.9.0"></script>
      <!-- BEGIN: AJAX CALLS-->
      <script>
  //Reset Password
function resetPassword(){
  $.ajax({
    type: "POST",
            url: "portal/admin/controller/process.php",
            data: $('form#resetpassword').serialize(),
            cache: false,
			success: function(response){
        var json = $.parseJSON(response);
        // console.log(json.Action);
        if(json.Action == 1) {
        $('.reset-password').empty(); //clear apend
         $('.reset-password').append(json.Message);
              $(".modal").on("hidden.bs.modal", function() {
              $("#reset-password").html("");
            });
          setTimeout(function(){ window.location="portal/admin-login"; },2000);
        }else {
            $('.reset-password').empty(); //clear apend
          $('.reset-password').append(json.Message);
               $(".modal").on("hidden.bs.modal", function() {
                $("#reset-password").html("");
            });

        }
        
			},
      error: function(jqXHR, textStatus, errorThrown) {
          //  console.log(jqXHR, textStatus, errorThrown);
          $('#reset-password').empty(); //clear apen
          $('.reset-password').append(errorThrown);
               $(".modal").on("hidden.bs.modal", function() {
                $("#forgot-password").h
            });
        }
		});
}
</script>


</html>