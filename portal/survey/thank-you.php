<?php
include_once('../../utils/EcoCashHoldingsCxUtility.php');
require_once('includes/header.php');
?>

<body class="nk-body bg-lighter npc-general has-sidebar ">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">

            <!-- wrap @s -->
            <div class="nk-wrap ">
                <!-- main header @s -->
                <!-- <div class="nk-header nk-header-fixed is-light">
                    <div class="container-fluid">
                        <div class="nk-header-wrap">
                            <div class="nk-menu-trigger d-xl-none ml-n1">
                                <a href="#" class="nk-nav-toggle nk-quick-nav-icon" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
                            </div>
                            <div class="nk-header-brand d-xl-none">
                                <a href="html/index.html" class="logo-link">
                                    <img class="logo-light logo-img" src="./images/vaya_logo_white.png" srcset="./images/vaya_logo_white.png 2x" alt="logo">
                                    <img class="logo-dark logo-img" src="./images/vaya_logo_white.png" srcset="./images/vaya_logo_white.pngg 2x" alt="logo-dark">
                                </a>
                            </div>
                            
                          
                         
                        </div>
                    </div>
                </div> -->
                <!-- main header @e -->
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="container-fluid">
                        <div class="nk-content-inner">
                            <div class="nk-content-body">
                                <div class="components-preview wide-md mx-auto">
                                    <div class="nk-block-head nk-block-head-lg wide-md">
                                        <div class="card text-white bg-secondary">
                                            <div class="card-inner">
                                                <h2 style="text-align: center;" class="card-title">Thank you for participating in our Customer Experience Survey.We value your feedback !!!</h2>
                                                <!-- <p class="card-text">The business would like to get feedback on ways of further enhancing our processes in a bid to satisfy our customers. May you kindly respond to the questions as accurately as possible. Your answers will be kept anonymous.</p> -->
                                            </div>
                                        </div>

                                    </div><!-- .nk-block-head -->
                                    <div class="nk-block nk-block-lg npc-general pg-auth">

                                    </div><!-- .nk-block -->

                                </div><!-- .components-preview -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- content @e -->
                <!-- footer @s -->
                <div class="nk-footer">
                    <div class="container-fluid">
                        <div class="nk-footer-wrap">
                            <div class="nk-footer-copyright"> &copy; Copyright Ecocash Holdings Zimbabwe 2022 <a href="https://www.ecocashholdings.co.zw/" target="_blank"></a>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- footer @e -->
            </div>
            <!-- wrap @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->

    <!-- JavaScript -->
    <script src="./assets/js/bundle.js"></script>
    <script src="./assets/js/scripts.js"></script>
</body>
</html>