<div class="nk-sidebar nk-sidebar-fixed is-dark " data-content="sidebarMenu">
                <div class="nk-sidebar-element nk-sidebar-head">
                    <div class="nk-menu-trigger">
                        <a href="#" class="nk-nav-toggle nk-quick-nav-icon d-xl-none" data-target="sidebarMenu"><em class="icon ni ni-arrow-left"></em></a>
                        <a href="#" class="nk-nav-compact nk-quick-nav-icon d-none d-xl-inline-flex" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
                    </div>
                    <div class="nk-sidebar-brand">
                        <a href="portal/admin/bookings" class="logo-link nk-sidebar-logo">
                        <h4 style="color:#fff;"> <span class="nk-menu-text">ADMINISTRATOR</span></h4>
                            <!-- <img class="logo-light logo-img"  src="./images/vaya_logo_white.png" srcset="./images/vaya_logo_white.png" alt="logo">
                            <img class="logo-dark logo-img" src="./images/vaya_logo_white.png" srcset="./images/vaya_logo_white.png" alt="logo-dark"> -->
                        </a>
                    </div>
                </div><!-- .nk-sidebar-element -->
                <div class="nk-sidebar-element nk-sidebar-body">
                    <div class="nk-sidebar-content">
                        <div class="nk-sidebar-menu" data-simplebar>
                            <ul class="nk-menu">
                                <!-- <li class="nk-menu-item">
                                    <a href="portal/admin/index" class="nk-menu-link">
                                        <span class="nk-menu-icon"><em class="icon ni ni-dashboard-fill"></em></span>
                                        <span class="nk-menu-text">Dashboard</span>
                                    </a>
                                </li> -->
                                <!-- .nk-menu-item -->
                                <li class="nk-menu-item has-sub">
                                    <a href="portal/admin/bookings" class="nk-menu-link nk-menu-toggle">
                                        <span class="nk-menu-icon"><em class="icon ni ni-calendar-booking-fill"></em></span>
                                        <span class="nk-menu-text">Bookings</span>
                                    </a>
                                    <ul class="nk-menu-sub">
                                        <li class="nk-menu-item">
                                            <a href="portal/admin/bookings" class="nk-menu-link"><span class="nk-menu-text">All Bookings</span></a>
                                            <a href="portal/admin/payments" class="nk-menu-link"><span class="nk-menu-text">All Payments</span></a>
                                            <!-- <a href="portal/admin/cash-payments" class="nk-menu-link"><span class="nk-menu-text">Cash Payments</span></a>
                                            <a href="portal/admin/eco-payments" class="nk-menu-link"><span class="nk-menu-text">EcoCash Payments</span></a> -->
                                        </li>
                                    </ul>
                                </li><!-- .nk-menu-item -->
                                <li class="nk-menu-item has-sub">
                                    <a href="#" class="nk-menu-link nk-menu-toggle">
                                        <span class="nk-menu-icon"><em class="icon ni ni-cart-fill"></em></span>
                                        <span class="nk-menu-text">Sales</span>
                                    </a>
                                    <ul class="nk-menu-sub">
                                        <li class="nk-menu-item">
                                            <a href="portal/admin/quotations" class="nk-menu-link"><span class="nk-menu-text">Quotations</span></a>
                                        </li>
                                        <!-- <li class="nk-menu-item">
                                            <a href="portal/admin/invoices" class="nk-menu-link"><span class="nk-menu-text">Invoices</span></a>
                                        </li> -->
                                    </ul>
                                   
                                </li>
                             
                                <li class="nk-menu-item">
                                    <a href="portal/admin/vehicles" class="nk-menu-link">
                                    <span class="nk-menu-icon"><em class="icon ni ni-truck"></em></span>
                                        <span class="nk-menu-text">Vehicles</span>
                                    </a>
                                </li><!-- .nk-menu-item -->
                                <!-- <li class="nk-menu-item">
                                    <a href="portal/admin/drivers" class="nk-menu-link">
                                        <span class="nk-menu-icon"><em class="icon ni ni-user-list-fill"></em></span>
                                        <span class="nk-menu-text">Drivers</span>
                                    </a>
                                </li> -->
                                <!-- .nk-menu-item -->
                                <li class="nk-menu-item">
                                    <a href="portal/admin/franchises" class="nk-menu-link">
                                    <span class="nk-menu-icon"><em class="icon ni ni-users-fill"></em></span>
                                        <span class="nk-menu-text">Franchises</span>
                                    </a>
                                </li><!-- .nk-menu-item -->
                                <li class="nk-menu-item has-sub">
                                    <a href="portal/admin/collection-sub-type" class="nk-menu-link nk-menu-toggle">
                                        <span class="nk-menu-icon"><em class="icon ni ni-home-fill"></em></span>
                                        <span class="nk-menu-text">Service</span>
                                    </a>
                                    <ul class="nk-menu-sub">
                                        <!-- <li class="nk-menu-item">
                                            <a href="portal/admin/waste-type" class="nk-menu-link"><span class="nk-menu-text">Waste Type</span></a>
                                        </li> -->
                                        <li class="nk-menu-item">
                                            <a href="portal/admin/collection-sub-type" class="nk-menu-link"><span class="nk-menu-text">Collection Sub Type</span></a>
                                        </li>
                                        <!-- <li class="nk-menu-item">
                                            <a href="portal/admin/collection-type" class="nk-menu-link"><span class="nk-menu-text">Collection Type</span></a>
                                        </li> -->
                                    </ul>
                                </li><!-- .nk-menu-item -->
                                <li class="nk-menu-item has-sub">
                                    <a href="portal/admin/currency-list" class="nk-menu-link nk-menu-toggle">
                                        <span class="nk-menu-icon"><em class="icon ni ni-coins"></em></span>
                                        <span class="nk-menu-text">Currency</span>
                                    </a>
                                    <ul class="nk-menu-sub">
                                        <!-- <li class="nk-menu-item">
                                            <a href="portal/admin/payment-methods" class="nk-menu-link"><span class="nk-menu-text">Payment Methods</span></a>
                                        </li> -->
                                        <li class="nk-menu-item">
                                            <a href="portal/admin/currency-list" class="nk-menu-link"><span class="nk-menu-text">Currency List</span></a>
                                        </li>
                                        <!-- <li class="nk-menu-item">
                                            <a href="portal/admin/invoice-list" class="nk-menu-link"><span class="nk-menu-text">Invocie List</span></a>
                                        </li>
                                        <li class="nk-menu-item">
                                            <a href="portal/admin/invoice-details" class="nk-menu-link"><span class="nk-menu-text">Invocie Details</span></a>
                                        </li> -->
                                    </ul>
                                </li><!-- .nk-menu-item -->
                                <!-- <li class="nk-menu-item has-sub">
                                    <a href="portal/admin/city-list" class="nk-menu-link nk-menu-toggle">
                                    <span class="nk-menu-icon"><em class="icon ni ni-list-index-fill"></em></span>
                                        <span class="nk-menu-text">Cities</span>
                                    </a>
                                    <ul class="nk-menu-sub">
                                  
                                        <li class="nk-menu-item">
                                            <a href="portal/admin/city-list" class="nk-menu-link"><span class="nk-menu-text">All Cities</span></a>
                                        </li>
                               
                                  
                                    </ul>
                                </li> -->
                                <!-- .nk-menu-item -->
                       
                                <li class="nk-menu-item has-sub">
                                    <a href="portal/admin/admins-list" class="nk-menu-link nk-menu-toggle">
                                        <span class="nk-menu-icon"><em class="icon ni ni-coin"></em></span>
                                        <span class="nk-menu-text">User Manage</span>
                                    </a>
                                    <ul class="nk-menu-sub">
                                    <li class="nk-menu-item">
                                            <a href="portal/admin/admins-list" class="nk-menu-link"><span class="nk-menu-text">Admins</span></a>
                                        </li>
                                        <li class="nk-menu-item">
                                            <a href="portal/admin/drivers-list" class="nk-menu-link"><span class="nk-menu-text">Drivers</span></a>
                                        </li>
                                        <li class="nk-menu-item">
                                            <a href="portal/admin/customer-list" class="nk-menu-link"><span class="nk-menu-text">Customers</span></a>
                                        </li>
                                    </ul><!-- .nk-menu-sub -->
                                </li><!-- .nk-menu-item -->
                            
                                <li class="nk-menu-item has-sub">
                                    <a href="portal/admin/client-report" class="nk-menu-link nk-menu-toggle">
                                        <span class="nk-menu-icon"><em class="icon ni ni-growth-fill"></em></span>
                                        <span class="nk-menu-text">Report</span>
                                    </a>
                                    <ul class="nk-menu-sub">
                                        <!-- <li class="nk-menu-item">
                                            <a href="html/crm/dealing-info.html" class="nk-menu-link"><span class="nk-menu-text">Dealing Info</span></a>
                                        </li> -->
                                        <li class="nk-menu-item">
                                            <a href="portal/admin/client-report" class="nk-menu-link"><span class="nk-menu-text">Client Report</span></a>
                                        </li>
                                        <!-- <li class="nk-menu-item">
                                            <a href="portal/admin/expense-report" class="nk-menu-link"><span class="nk-menu-text">Expense Report</span></a>
                                        </li> -->
                                    </ul><!-- .nk-menu-sub -->
                                </li><!-- .nk-menu-item -->
                              
                            
                                <li class="nk-menu-item">
                                    <a href="portal/admin/support" class="nk-menu-link">
                                        <span class="nk-menu-icon"><em class="icon ni ni-chat-circle-fill"></em></span>
                                        <span class="nk-menu-text">Support</span>
                                    </a>
                                </li><!-- .nk-menu-item -->
                                <!-- <li class="nk-menu-item">
                                    <a href="portal/admin/settings" class="nk-menu-link">
                                        <span class="nk-menu-icon"><em class="icon ni ni-setting-alt-fill"></em></span>
                                        <span class="nk-menu-text">Settings</span>
                                    </a>
                                </li> -->
                                <!-- .nk-menu-item -->
                          
                            </ul><!-- .nk-menu -->
                        </div><!-- .nk-sidebar-menu -->
                    </div><!-- .nk-sidebar-content -->
                </div><!-- .nk-sidebar-element -->
            </div>