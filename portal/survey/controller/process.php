<?php
include_once('../../../utils/EcoCashHoldingsCxUtility.php');


//add survey
if (isset($_POST['add_survey'])) {
    $surveyName = $_POST['surveyName'];
    $status = 'CLOSED';
    $add_survey_result = addSurvey($surveyName,$status);
    $add_survey_data = json_decode($add_survey_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_survey_data);
    exit;
}

//add service
if (isset($_POST['add_service'])) {
    $serviceName = $_POST['serviceName'];
    $status = $_POST['status'];
    $createdBy = $_POST['createdBy'];
    $add_service_result = addService($serviceName,$status,$createdBy);
    $add_service_data = json_decode($add_service_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_service_data);
    exit;
}


//add config
if (isset($_POST['add_config'])) {
    $name = $_POST['name'];
    $value = $_POST['value'];
    $add_config_result = addConfig($name,$value);
    $add_config_data = json_decode($add_config_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_config_data);
    exit;
}

//add user
if (isset($_POST['add_user'])) {
    $userName = $_POST['userName'];
    $firstName = $_POST['firstName'];
    $lastName = $_POST['lastName'];
    $email = $_POST['email'];
    $mobileNumber = $_POST['mobileNumber'];
    $accessType = $_POST['accessType'];
    $createdBy = $_POST['createdBy'];

    $add_user_result = addUser($userName,$firstName,$lastName,$email,$mobileNumber,$accessType,$createdBy);
    $add_user_data = json_decode($add_user_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_user_data);
    exit;
}

//add Rating Limit
if (isset($_POST['add-rate-limit'])) {
    $configId = $_POST['configId'];
    $rate = $_POST['rate'];
    $perNumberOfDays = $_POST['perNumberOfDays'];
    $lastModifiedBy = $_POST['lastModifiedBy'];
    $createdBy = $_POST['createdBy'];
    
    $add_limit_result = addLimit($configId,$rate,$perNumberOfDays,$lastModifiedBy,$createdBy);
    $add_limit_data = json_decode($add_limit_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_limit_data);
    exit;
}

//create question
if (isset($_POST['create_question'])) {
    $surveyId = $_POST['surveyId'];
    $questionNumber = $_POST['questionNumber'];
    $question = $_POST['question'];
    $createdBy = $_POST['createdBy'];
    $create_cquestion_result = createQuestion($surveyId,$questionNumber,$question,$createdBy);
    $create_cquestion_data = json_decode($create_cquestion_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($create_cquestion_data);
    exit;
}

//edit Survey
if (isset($_POST['edit_survey'])) {
    $surveyId = $_POST['surveyId'];
    $surveyName = $_POST['surveyName'];
    $status = $_POST['status'];
    $lastModifiedBy = $_POST['lastModifiedBy'];

    $edit_survey_result = editSurvey($surveyId,$surveyName,$status,$lastModifiedBy);
    $edit_survey_data = json_decode($edit_survey_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($edit_survey_data);
    exit;
}

//edit user
if (isset($_POST['edit_user'])) {
    $userId = $_POST['userId'];
    $userName = $_POST['userName'];
    $firstName = $_POST['firstName'];
    $lastName = $_POST['lastName'];
    $accessType = $_POST['accessType'];
    $email = $_POST['email'];
    $mobileNumber = $_POST['mobileNumber'];
    $lastModifiedBy = $_POST['lastModifiedBy'];
    $status = $_POST['status'];
    
    $edit_user_result = editUser($status,$userId,$userName,$firstName,$lastName,$accessType,$email,$mobileNumber,$lastModifiedBy);
    $edit_user_data = json_decode($edit_user_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($edit_user_data);
    exit;
}

//edit Service
if (isset($_POST['edit_service'])) {
    $serviceId = $_POST['serviceId'];
    $serviceName = $_POST['serviceName'];
    $status = $_POST['status'];
    $ratingLimit = $_POST['ratingLimit'];
    $lastModifiedBy = $_POST['lastModifiedBy'];

    $edit_service_result = editService($serviceId,$serviceName,$status,$ratingLimit,$lastModifiedBy);
    $edit_service_data = json_decode($edit_service_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($edit_service_data);
    exit;
}

//cred Service
if (isset($_POST['cred_service'])) {
    $serviceName = $_POST['serviceName'];

    $cred_service_result = credService($serviceName);
    $cred_service_data = json_decode($cred_service_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($cred_service_data);
    exit;
}

//send cred 
if (isset($_POST['send_cred'])) {
    $serviceName = $_POST['serviceName'];
    $email = $_POST['email'];

    $cred_send_result = sendCred($serviceName,$email);
    $cred_send_data = json_decode($cred_send_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($cred_send_data);
    exit;
}


    //edit question
if (isset($_POST['edit_question'])) {
    $surveyId = $_POST['surveyId'];
    $lastModifiedBy = $_POST['lastModifiedBy'];

    $question1 = $_POST['question1'];
    $questionId1 = $_POST['questionId1'];
    editQuestion($questionId1,$surveyId,1,$question1,$lastModifiedBy);

    $question2 = $_POST['question2'];
    $questionId2 = $_POST['questionId2'];
    editQuestion($questionId2,$surveyId,2,$question2,$lastModifiedBy);

    $question3 = $_POST['question3'];
    $questionId3 = $_POST['questionId3'];
    editQuestion($questionId3,$surveyId,3,$question3,$lastModifiedBy);

    $question4 = $_POST['question4'];
    $questionId4 = $_POST['questionId4'];

 
    $edit_question_result = editQuestion($questionId4,$surveyId,4,$question4,$lastModifiedBy);
    $edit_question_data = json_decode($edit_question_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($edit_question_data);
    exit;
    
}
//edit Config
if (isset($_POST['edit_config'])) {
    $configId = $_POST['configId'];
    $name = $_POST['name'];
    $value = $_POST['value'];
    $lastModifiedBy = $_POST['lastModifiedBy'];

 
    $edit_config_result = editConfig($configId,$name,$value,$lastModifiedBy);
    $edit_config_data = json_decode($edit_config_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($edit_config_data);
    exit;
}


//save User Responses
if (isset($_POST['save_user_responses'])) {
    
    $responseOne = $_POST['responseOne']??"";
    $responseTwo = $_POST['responseTwo']??"";
    $responseThree = $_POST['responseThree']??"";
    $responseFour = $_POST['responseFour']??"";

    $questionIdOne = $_POST['questionIdOne']??"";
    $questionIdTwo = $_POST['questionIdTwo']??"";
    $questionIdThree = $_POST['questionIdThree']??"";
    $questionIdFour = $_POST['questionIdFour']??"";

    $msisdn = $_POST['msisdn']??"";
    $uuid = $_POST['uuid']??"";
    $serviceName = $_POST['serviceName']??"";
    $additionalTextOne = $_POST['additionalTextOne']??"";
    $additionalTextTwo = $_POST['additionalTextTwo']??"";
    $additionalTextThree = $_POST['additionalTextThree']??"";
    $additionalTextFour = $_POST['additionalTextFour']??"";

 
    $save_responses_result = saveAllResponses($uuid,$serviceName,$questionIdOne,$responseOne,$additionalTextOne,$questionIdTwo,$responseTwo,$additionalTextTwo,$questionIdThree,$responseThree,$additionalTextThree,$questionIdFour,$responseFour,$additionalTextFour);
    $save_responses_data = json_decode($save_responses_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($save_responses_data);
    exit;

   
}

//Delete Raring Limit
if (isset($_POST['delete_limit'])) {
    $configId = $_POST['configId'];
    
    $delete_limit_result = deleteLimit($configId);
    $delete_limit_data = json_decode($delete_limit_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($delete_limit_data);
    exit;
}

 
?>
