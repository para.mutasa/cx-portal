<?php
session_start();
include_once('../utils/EcoCashHoldingsCxUtility.php');
if (isset($_POST['change_password'])) {
    if (!empty($_POST['email']) && !empty($_POST['password']) && !empty($_POST['oldPassword'])) {
        $email          = trim($_POST['email']);
        $password      = trim($_POST['password']);
        $oldPassword      = trim($_POST['oldPassword']);

        $changePassword = json_decode(changePassword($email,$oldPassword,$password),true);

        if($changePassword['responseStatus'] == "SUCCESS"){
            header("location: admin-login");
            exit;
                
        } else {
           
            $errorMsg = $changePassword['responseMessage'];
       
     }
       
    }    
               
          
    
}

?>

<!DOCTYPE html>
<html lang="zxx" class="js">

<head>
    <base href="../">
    <meta charset="utf-8">
    <meta name="author" content="Softnio">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="clean city.">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="./images/favicon.png">
    <!-- Page Title  -->
    <title>EcoCash Holdings - Leading PAN Africa Technology solutions group</title>
    <!-- StyleSheets  -->
    <link rel="stylesheet" href="./assets/css/dashlite.css?ver=2.9.0">
    <link id="skin-default" rel="stylesheet" href="./assets/css/theme.css?ver=2.9.0">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js"></script>
    <script>
        function AES_CBC_encrypt() {
            const key = 'b!5Tb+EjdKyaYj6{M/atWSdkghjd8945'
            // var iv = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15 ]
            var iv = "1234567890000000";
            var message = document.getElementById("password").value;
            // utf8 character string —>WordArray object ,WordArray It's a save 32 An array of bit integers , Equivalent to binary
            let keyHex = CryptoJS.enc.Utf8.parse(key); //
            let ivHex = CryptoJS.enc.Utf8.parse(iv);
            let messageHex = CryptoJS.enc.Utf8.parse(message);
            let encrypted = CryptoJS.AES.encrypt(messageHex, keyHex, {
                iv: ivHex,
                mode: CryptoJS.mode.CBC,
                padding: CryptoJS.pad.Pkcs7
            });
            document.getElementById('password').value = encrypted.toString();
            return true;
            // return encrypted.toString();// base64 result
            //return encrypted.ciphertext.toString(); // Binary results
        }
    </script>
    
    <style>
        .btn-primary {
            color: #fff;
            background-color: #888888;
            border-color: #888888;

        }

        body {
            background: url("./images/stock/eco_image.jpg") no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
    </style>
</head>

<body class="nk-body bg-white npc-general pg-auth">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <!-- wrap @s -->
            <div class="nk-wrap nk-wrap-nosidebar">
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="nk-block nk-block-middle nk-auth-body  wide-xs">
                        <div class="brand-logo pb-4 text-center">
                            <a href="html/index.html" class="logo-link">
                            </a>
                        </div>
                        <div class="card card-bordered">
                            <div class="card-inner card-inner-lg">
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <!-- <img class="logo-light logo-img logo-img-md" src="./images/ecocash_logo.png" srcset="./images/ecocash_logo.png" alt="logo"> -->
                                        <img class="logo-dark logo-img logo-img-md " src="./images/ecocash_logo.png" srcset="./images/ecocash_logo.png" alt="logo-dark">
                                        <h4 class="nk-block-title">Change Password</h4>
                                        <div class="nk-block-des">
                                            <p>Change Your Password</p>
                                        </div>
                                    </div>
                                    <?php

                                    if (isset($errorMsg)) {

                                        echo '<span style ="margin-left: -1%;"class="alert alert-danger">';

                                        echo $errorMsg;

                                        echo '</span>';

                                        unset($errorMsg);
                                    }

                                    ?>
                                </div>
                                <form action="portal/change-password" method="post">
                                    <div class="form-group">
                                        <div class="form-label-group">
                                            <label class="form-label" for="default-01">Email</label>
                                        </div>
                                        <div class="form-control-wrap">
                                            <input type="text" class="form-control form-control-lg" id="default-01" name="email" placeholder="Enter your email address" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-label-group">
                                            <label class="form-label" for="default-01">Old Password</label>

                                        </div>
                                        <div class="form-control-wrap">
                                            <input type="password" name="oldPassword" class="form-control form-control-lg" id="oldPassword" placeholder="Enter your Old Password" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-label-group">
                                            <label class="form-label" for="password">New Password</label>

                                        </div>
                                        <div class="form-control-wrap">
                                            <a href="#" class="form-icon form-icon-right passcode-switch lg" data-target="password">
                                                <em class="passcode-icon icon-show icon ni ni-eye"></em>
                                                <em class="passcode-icon icon-hide icon ni ni-eye-off"></em>
                                            </a>
                                            <input type="password" class="form-control form-control-lg" id="password" placeholder="Enter your password" name="password" required>
                                        </div><br />
                                        <!-- <div class="g-recaptcha" data-sitekey="6LeUkr8ZAAAAAMC0ZmiMkM2tRZ_1sucyFY1WV_71"></div> -->
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-lg btn-primary btn-block" onclick="return AES_CBC_encrypt()" name="change_password">Change Password</button>
                                    </div>
                                </form>
                                <!-- <div class="form-note-s2 text-center pt-4"> New on our platform? <a href="portal/register">Create an account</a> -->
                            </div>
                            <!-- <div class="text-center pt-4 pb-3">
                                    <h6 class="overline-title overline-title-sap"><span>OR</span></h6>
                                </div>
                                <ul class="nav justify-center gx-4">
                                    <li class="nav-item"><a class="nav-link" href="#">Facebook</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#">Google</a></li>
                                </ul> -->
                        </div>
                    </div>
                </div>
                <div class="nk-footer nk-auth-footer-full">
                    <div class="container wide-lg">
                        <!-- <div class="row g-3">
                                <div class="col-lg-6 order-lg-last">
                                    <ul class="nav nav-sm justify-content-center justify-content-lg-end">
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">Terms & Condition</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">Privacy Policy</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">Help</a>
                                        </li>
                                   
                                    </ul>
                                </div> -->
                        <div class="col-lg-6">
                            <div class="nk-block-content text-center text-lg-left">
                                <p> &copy; Copyright Ecocash Holdings Zimbabwe 2022 <a href="https://www.ecocashholdings.co.zw/" target="_blank"></a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- wrap @e -->
    </div>
    <!-- content @e -->
    </div>
    <!-- main @e -->
    </div>
    <!-- app-root @e -->
    <!-- JavaScript -->
    <script src="./assets/js/bundle.js"></script>
    <script src="./assets/js/scripts.js"></script>
    <!-- Google reCaptcha -->
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>


</html>