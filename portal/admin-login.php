<?php
   session_start();
    include_once('../utils/EcoCashHoldingsCxUtility.php');
    if(isset($_POST['login']))
    {
        if(!empty($_POST['email']) && !empty(trim($_POST['password'])))
        {
            $email 		 = trim($_POST['email']);
            $password 	 = trim($_POST['password']);

            // var_dump($password);
            // exit;

        if (!isset($_POST['g-recaptcha-response']) || empty($_POST['g-recaptcha-response'])) {
            $errorMsg = 'reCAPTHCA verification failed, please try again.';
        } else {

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://www.google.com/recaptcha/api/siteverify?secret=' . $secret . '&response=' . $_POST['g-recaptcha-response']);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($response);

            if ($response->success) {
                $getUser = json_decode(adminLogin($email, $password), true);


                if ($getUser['responseStatus'] == "SUCCESS") {
                    $_SESSION["access_type"] = $getUser['eobjResponse']['accessType'];
                    $_SESSION["admin_id"] = $getUser['eobjResponse']['userId'];
                    $_SESSION["admin_vphone"] = $getUser['eobjResponse']['mobileNumber'];
                    $_SESSION["admin_Email"] = $getUser['eobjResponse']['email'];
                    $_SESSION["first_name"] = $getUser['eobjResponse']['firstName'];
                    $_SESSION["last_name"] = $getUser['eobjResponse']['lastName'];
                    $_SESSION["changePasswordOnFirstLogin"] = $getUser['eobjResponse']['changePasswordOnFirstLogin'];


                    if ($_SESSION["access_type"] == "SUPERADMIN" &&  $_SESSION["changePasswordOnFirstLogin"] == "CHANGED" ) {
                        header('location: admin/surveys');
                        exit;
                    } else if ($_SESSION["access_type"] == "SUPERADMIN" &&  $_SESSION["changePasswordOnFirstLogin"] != "CHANGED" ) {
                        header('location: change-password');
                    }  else if ($_SESSION["access_type"] == "ADMIN" &&  $_SESSION["changePasswordOnFirstLogin"] == "CHANGED" ) {
                        header('location: admin/surveys');
                    } else if ($_SESSION["access_type"] == "ADMIN" &&  $_SESSION["changePasswordOnFirstLogin"] != "CHANGED" ) {
                        header('location: change-password');
                    }else {
                        header("location: portal/admin-login");
                        exit;
                    }
                } else {
                    session_destroy();
                    $errorMsg = $getUser['responseMessage'];
                }
            } else {
                $errorMsg = 'reCAPTHCA verification failed, please try again.';
            }
        }

        if (isset($_GET['logout']) && $_GET['logout'] == true) {
            session_destroy();
            header("location: portal/admin-login");
            exit;
        }

        if (isset($_GET['lmsg']) && $_GET['lmsg'] == true) {
            $errorMsg = "Login required to access dashboard";
        }
    }
}

?>

<!DOCTYPE html>
<html lang="zxx" class="js">

<head>
    <base href="../">
    <meta charset="utf-8">
    <meta name="author" content="Softnio">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="clean city.">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="./images/favicon.png">
    <!-- Page Title  -->
    <title>EcoCash Holdings - Leading PAN Africa Technology solutions group</title>
    <!-- StyleSheets  -->
    <link rel="stylesheet" href="./assets/css/dashlite.css?ver=2.9.0">
    <link id="skin-default" rel="stylesheet" href="./assets/css/theme.css?ver=2.9.0">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js"></script>
    <script>
    function AES_CBC_encrypt() {
    const key = 'b!5Tb+EjdKyaYj6{M/atWSdkghjd8945'
    // var iv = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15 ]
    var iv = "1234567890000000";
    var message = document.getElementById("password").value;
    // utf8 character string —>WordArray object ,WordArray It's a save 32 An array of bit integers , Equivalent to binary
    let keyHex = CryptoJS.enc.Utf8.parse(key); //
    let ivHex = CryptoJS.enc.Utf8.parse(iv);
    let messageHex = CryptoJS.enc.Utf8.parse(message);
    let encrypted = CryptoJS.AES.encrypt(messageHex, keyHex, {
    iv: ivHex,
    mode: CryptoJS.mode.CBC,
    padding: CryptoJS.pad.Pkcs7
    });
    document.getElementById('password').value= encrypted.toString();
    return true;
    // return encrypted.toString();// base64 result
    //return encrypted.ciphertext.toString(); // Binary results
    }
    </script>
    <style>
        .btn-primary {
            color: #fff;
            background-color: #888888;
            border-color: #888888;

        }

        body {
            background: url("./images/stock/eco_image.jpg") no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
    </style>
</head>

<body class="nk-body bg-white npc-general pg-auth">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <!-- wrap @s -->
            <div class="nk-wrap nk-wrap-nosidebar">
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="nk-block nk-block-middle nk-auth-body  wide-xs">
                        <div class="brand-logo pb-4 text-center">
                            <a href="html/index.html" class="logo-link">
                            </a>
                        </div>
                        <div class="card card-bordered">
                            <div class="card-inner card-inner-lg">
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <!-- <img class="logo-light logo-img logo-img-md" src="./images/ecocash_logo.png" srcset="./images/ecocash_logo.png" alt="logo"> -->
                                        <img class="logo-dark logo-img logo-img-md " src="./images/ecocash_logo.png" srcset="./images/ecocash_logo.png" alt="logo-dark">
                                        <h4 class="nk-block-title">Sign-In</h4>
                                        <div class="nk-block-des">
                                            <p>Access the CX portal using your email and password.</p>
                                        </div>
                                    </div>
                                    <?php

if(isset($errorMsg))

{

echo '<span style ="margin-left: -1%;"class="alert alert-danger">';

echo $errorMsg;

echo '</span>';

unset($errorMsg);

}

?>
                                </div>
                                <form action="portal/admin-login" method="post">
                                    <div class="form-group">
                                        <div class="form-label-group">
                                            <label class="form-label" for="default-01">Email</label>
                                        </div>
                                        <div class="form-control-wrap">
                                            <input type="text" class="form-control form-control-lg" id="default-01" name="email" placeholder="Enter your email address" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-label-group">
                                            <label class="form-label" for="password">Password</label>
                                            <a class="link link-primary link-sm" href="portal/admin-forgot-password">Forgot Password?</a>
                                        </div>
                                        <div class="form-control-wrap">
                                            <a href="#" class="form-icon form-icon-right passcode-switch lg" data-target="password">
                                                <em class="passcode-icon icon-show icon ni ni-eye"></em>
                                                <em class="passcode-icon icon-hide icon ni ni-eye-off"></em>
                                            </a>
                                            <input type="password" class="form-control form-control-lg" id="password" placeholder="Enter your password" name="password" required>
                                        </div><br />
                                        <div class="g-recaptcha" data-sitekey="6LeUkr8ZAAAAAMC0ZmiMkM2tRZ_1sucyFY1WV_71"></div>
                                    </div>
                                    <div class="form-group">
                                    <button type="submit"  class="btn btn-lg btn-primary btn-block"   onclick="return AES_CBC_encrypt()" name="login" >Sign in</button>
                                    </div>
                                </form>
                                <!-- <div class="form-note-s2 text-center pt-4"> New on our platform? <a href="portal/register">Create an account</a> -->
                            </div>
                            <!-- <div class="text-center pt-4 pb-3">
                                    <h6 class="overline-title overline-title-sap"><span>OR</span></h6>
                                </div>
                                <ul class="nav justify-center gx-4">
                                    <li class="nav-item"><a class="nav-link" href="#">Facebook</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#">Google</a></li>
                                </ul> -->
                        </div>
                    </div>
                </div>
                <div class="nk-footer nk-auth-footer-full">
                    <div class="container wide-lg">
                        <!-- <div class="row g-3">
                                <div class="col-lg-6 order-lg-last">
                                    <ul class="nav nav-sm justify-content-center justify-content-lg-end">
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">Terms & Condition</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">Privacy Policy</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">Help</a>
                                        </li>
                                   
                                    </ul>
                                </div> -->
                        <div class="col-lg-6">
                            <div class="nk-block-content text-center text-lg-left">
                                <p> &copy; Copyright Ecocash Holdings Zimbabwe 2022 <a href="https://www.ecocashholdings.co.zw/" target="_blank"></a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- wrap @e -->
    </div>
    <!-- content @e -->
    </div>
    <!-- main @e -->
    </div>
    <!-- app-root @e -->
    <!-- JavaScript -->
    <script src="./assets/js/bundle.js"></script>
    <script src="./assets/js/scripts.js"></script>
    <!-- Google reCaptcha -->
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <!-- select region modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="region">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                <div class="modal-body modal-body-md">
                    <h5 class="title mb-4">Select Your Country</h5>
                    <div class="nk-country-region">
                        <ul class="country-list text-center gy-2">
                            <li>
                                <a href="#" class="country-item">
                                    <img src="./images/flags/arg.png" alt="" class="country-flag">
                                    <span class="country-name">Argentina</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="country-item">
                                    <img src="./images/flags/aus.png" alt="" class="country-flag">
                                    <span class="country-name">Australia</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="country-item">
                                    <img src="./images/flags/bangladesh.png" alt="" class="country-flag">
                                    <span class="country-name">Bangladesh</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="country-item">
                                    <img src="./images/flags/canada.png" alt="" class="country-flag">
                                    <span class="country-name">Canada <small>(English)</small></span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="country-item">
                                    <img src="./images/flags/china.png" alt="" class="country-flag">
                                    <span class="country-name">Centrafricaine</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="country-item">
                                    <img src="./images/flags/china.png" alt="" class="country-flag">
                                    <span class="country-name">China</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="country-item">
                                    <img src="./images/flags/french.png" alt="" class="country-flag">
                                    <span class="country-name">France</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="country-item">
                                    <img src="./images/flags/germany.png" alt="" class="country-flag">
                                    <span class="country-name">Germany</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="country-item">
                                    <img src="./images/flags/iran.png" alt="" class="country-flag">
                                    <span class="country-name">Iran</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="country-item">
                                    <img src="./images/flags/italy.png" alt="" class="country-flag">
                                    <span class="country-name">Italy</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="country-item">
                                    <img src="./images/flags/mexico.png" alt="" class="country-flag">
                                    <span class="country-name">México</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="country-item">
                                    <img src="./images/flags/philipine.png" alt="" class="country-flag">
                                    <span class="country-name">Philippines</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="country-item">
                                    <img src="./images/flags/portugal.png" alt="" class="country-flag">
                                    <span class="country-name">Portugal</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="country-item">
                                    <img src="./images/flags/s-africa.png" alt="" class="country-flag">
                                    <span class="country-name">South Africa</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="country-item">
                                    <img src="./images/flags/spanish.png" alt="" class="country-flag">
                                    <span class="country-name">Spain</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="country-item">
                                    <img src="./images/flags/switzerland.png" alt="" class="country-flag">
                                    <span class="country-name">Switzerland</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="country-item">
                                    <img src="./images/flags/uk.png" alt="" class="country-flag">
                                    <span class="country-name">United Kingdom</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="country-item">
                                    <img src="./images/flags/english.png" alt="" class="country-flag">
                                    <span class="country-name">United State</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div><!-- .modal-content -->
        </div><!-- .modla-dialog -->
    </div><!-- .modal -->

</html>